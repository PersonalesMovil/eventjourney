package Resource;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.selectacg.daniel.ibmeventinnovation.R;

/**
 * Created by SelectaCG on 22/07/15.
 */
public class Connectivity {

    private static Boolean isOnline;
    private static Context context;
    private static Connectivity instance;

    private Connectivity(Context context){
        this.context = context;
    }

    public static Connectivity newInstance(Context context) {
        if (instance == null){
            instance = new Connectivity(context);
            if(isConnectedNetWork(context)){
                instance.isOnline = true;
            }else{
                instance.isOnline = false;
            }
        }
        return instance;
    }

    public static Boolean isConnectedNetWork(Context context){
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager != null) {
                NetworkInfo activeConnection = connectivityManager.getActiveNetworkInfo();
                NetworkInfo.DetailedState statusConnection = activeConnection.getDetailedState();
                if (statusConnection.equals(NetworkInfo.DetailedState.CONNECTED)) {
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        }catch (Exception ex){
            return false;
        }
    }

    public static Boolean isConnectedWithWifi(Context context){ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo activeConnection = connectivityManager.getActiveNetworkInfo();
            if (activeConnection != null) {
                if (activeConnection.getType() == ConnectivityManager.TYPE_WIFI) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    public static Boolean isConnectedWithDataMobile(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo activeConnection = connectivityManager.getActiveNetworkInfo();
            if (activeConnection != null) {
                if (activeConnection.getType() == ConnectivityManager.TYPE_MOBILE) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

}
