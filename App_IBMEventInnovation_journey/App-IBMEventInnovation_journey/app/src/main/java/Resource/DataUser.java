package Resource;

import java.util.ArrayList;

import Model.Events;
import Model.Items;
import Model.Orders;
import Model.Photos;
import Model.Records;
import Model.Routers;
import Model.Settings;
import Model.Shops;
import Model.Users;

/**
 * Created by Daniel on 1/06/16.
 */
public class DataUser {

    public Users userConnect;
    public ArrayList<Orders> ordersList;
    public ArrayList<Records> recordsList;
    public ArrayList<Items> ItemsList;
    public ArrayList<Shops> ShopsList;
    public ArrayList<Routers> routersList;
    public ArrayList<Photos> photosSavedList;

    public Routers lastRouterRegister;

    public Settings settings;
    public Boolean loginWithFacebook;
    public Boolean loginwithTwitter;
    public Boolean loginManual;

    private static DataUser instance;

    private DataUser() {
        ItemsList = null;
        photosSavedList = new ArrayList<>();
    }

    public static DataUser newInstance() {
        if (instance == null) {
            instance = new DataUser();
        }
        return instance;
    }
}
