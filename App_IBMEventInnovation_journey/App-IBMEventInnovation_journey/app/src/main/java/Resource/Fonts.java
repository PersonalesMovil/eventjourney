package Resource;

import android.app.Activity;
import android.graphics.Typeface;

/**
 * Created by Daniel on 29/06/16.
 */
public class Fonts {

    public static Typeface obtenerFontHelveticaNeue(Activity activity){
        Typeface font = Typeface.createFromAsset(activity.getAssets(),"helvetica.ttf");
        return font;
    }

    public static Typeface obtenerFontHelveticaNeueLight(Activity activity){
        Typeface font = Typeface.createFromAsset(activity.getAssets(),"HelveticaNeue-Light.otf");
        return font;
    }

    public static Typeface obtenerFontHelveticaNeueMedium(Activity activity){
        Typeface font = Typeface.createFromAsset(activity.getAssets(),"HelveticaNeue-Medium.otf");
        return font;
    }

    public static Typeface obtenerFontHelveticaNeueRoman(Activity activity){
        Typeface font = Typeface.createFromAsset(activity.getAssets(),"HelveticaNeue-Roman.otf");
        return font;
    }

    public static long timeAnimations(){
        return 1000;
    }
}
