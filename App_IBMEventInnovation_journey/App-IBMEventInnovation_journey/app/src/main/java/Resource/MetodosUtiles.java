package Resource;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;


/**
 * Created by Daniel on 16/10/15.
 */
public class MetodosUtiles {

    public static Bitmap redimensionarImagenMaximo(Bitmap mBitmap, float newWidth, float newHeigth){
        try {
            //Redimensionamos
            int width = mBitmap.getWidth();
            int height = mBitmap.getHeight();
            float scaleWidth = ((float) newWidth) / width;
            float scaleHeight = ((float) newHeigth) / height;
            // create a matrix for the manipulation
            Matrix matrix = new Matrix();
            // resize the bit map
            matrix.postScale(scaleWidth, scaleHeight);
            // recreate the new Bitmap
            return Bitmap.createBitmap(mBitmap, 0, 0, width, height, matrix, false);
        }catch (Exception ex){
            throw  ex;
        }
    }

    public static byte[] convertBitMapToBase64(Bitmap bitmap, int quality){
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        }catch (Exception ex){
            throw  ex;
        }
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static String date_format(String format){
        try {
            SimpleDateFormat zonaHoraria = new SimpleDateFormat(format);
            String value = zonaHoraria.format(Calendar.getInstance().getTime());
            return value;
        }catch (Exception ex){
            throw ex;
        }
    }

    public static String date_format_toGregorian(GregorianCalendar calendar,String format){
        try {
            SimpleDateFormat timeFormat = new SimpleDateFormat(format);
            timeFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            String dateFormatted = timeFormat.format(calendar.getTime());
            return dateFormatted;
        }catch (Exception ex){
            throw  ex;
        }
    }

    public static void mostrarPausaMensaje(){
        try {
            Thread.sleep(2000);
        }catch (Exception e){
            Log.d("Error pausa",e.getMessage());
        }
    }

     public static void pausarProcesos(long time){
        try {
            Thread.sleep(time);
        }catch (Exception e){
            Log.d("Error pausa",e.getMessage());
        }
    }
}
