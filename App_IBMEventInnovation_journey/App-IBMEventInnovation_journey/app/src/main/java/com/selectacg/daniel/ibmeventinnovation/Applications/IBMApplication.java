package com.selectacg.daniel.ibmeventinnovation.Applications;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.firebase.client.Firebase;
import com.selectacg.daniel.ibmeventinnovation.R;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.wikitude.WikitudeSDKStartupConfiguration;
import com.wikitude.common.camera.CameraSettings;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Daniel on 19/06/16.
 */
public class IBMApplication  extends Application {

    private static final String TWITTER_KEY = "Vp9oR7F9dCdO5ltmbILTv8Q84";
    private static final String TWITTER_SECRET = "6Ajd3m3HDc9hEeMsFdry8VPwl0jhBl3jClJeiOjDN6DuyOqmHs";

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
            Fabric.with(this, new Twitter(authConfig));
            Firebase.setAndroidContext(this);
            Firebase.getDefaultConfig().setPersistenceEnabled(true);
            Firebase scoresRef = new Firebase("https://dinosaur-facts.firebaseio.com/scores");
            scoresRef.keepSynced(true);
        }catch (Exception ex){
            Log.e("LoginLoad", ex.getLocalizedMessage());
        }

        FacebookSdk.sdkInitialize(getApplicationContext());
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.selectacg.daniel.ibmeventinnovation",  // replace with your unique package Name
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
}
