package com.selectacg.daniel.ibmeventinnovation.Controllers;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.selectacg.daniel.ibmeventinnovation.R;

import java.util.ArrayList;

import Adapters.HistorialFilterAdapter;
import Model.Records;
import Resource.DataUser;
import Resource.Fonts;

/**
 * Created by Daniel on 16/06/16.
 */
public class HistorialesController extends Activity {

    EditText filterEditText;
    ListView miHistorialListView;
    ArrayList<Records> historialesList;
    HistorialFilterAdapter adapter;
    RelativeLayout contentProgressBar;

    TextView titleTextView;
    ImageView returnImageView;

    public HistorialesController(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.mi_historial);
            loadControllersFromView();
            loadArguments();
            loadEvents();
            loadFontAndAnimations();
        }catch (Exception ex){
            Log.d("HistorialesController","Error: " + ex.getMessage());
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        loadFontAndAnimations();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadFontAndAnimations();
    }

    private void loadControllersFromView(){
        // Inflate the layout for this fragment
        filterEditText = (EditText)findViewById(R.id.filterEditText);
        miHistorialListView = (ListView)findViewById(R.id.historialListView);
        contentProgressBar = (RelativeLayout) findViewById(R.id.contentProgressBar);
        contentProgressBar.setVisibility(View.INVISIBLE);

        titleTextView = (TextView) findViewById(R.id.titleTextView);
        returnImageView = (ImageView) findViewById(R.id.returnImageView);
    }

    private void loadFontAndAnimations(){
        try{
            Typeface helveticaNeueLight = Fonts.obtenerFontHelveticaNeueLight(this);
            Typeface helveticaMedium = Fonts.obtenerFontHelveticaNeueMedium(this);

            Animation translateBottomTop = AnimationUtils.loadAnimation(this, R.anim.translate_from_bottom_to_top);
            translateBottomTop.setDuration(Fonts.timeAnimations());

            titleTextView.setTypeface(helveticaMedium);
            translateBottomTop.reset();
            titleTextView.startAnimation(translateBottomTop);

            translateBottomTop.reset();
            returnImageView.startAnimation(translateBottomTop);

            filterEditText.setTypeface(helveticaNeueLight);
            translateBottomTop.reset();
            filterEditText.startAnimation(translateBottomTop);

        }catch (Exception ex){
            Log.d("LoadFontAndAnimations", "HistorialesController:" + ex.getLocalizedMessage());
        }
    }

    private void loadArguments(){
        historialesList = new ArrayList<>();
        DataUser dataUser = DataUser.newInstance();
        ArrayList<Records> allRecord = dataUser.recordsList;
        historialesList = allRecord;

        titleTextView.setText(getString(R.string.title_record_list));
        adapter = new HistorialFilterAdapter(this, historialesList);

        //Carga del adapter que contiene los itinerarios pendientes
        miHistorialListView.setAdapter(adapter);
    }

    private void loadEvents(){
        filterEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        miHistorialListView.setTextFilterEnabled(true);
        final Activity activity = this;
        miHistorialListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Records currentEvent = (Records) adapter.getItem(position);

                Intent MandantSelection = new Intent(activity, DescriptionController.class);

            }
        });

        returnImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
