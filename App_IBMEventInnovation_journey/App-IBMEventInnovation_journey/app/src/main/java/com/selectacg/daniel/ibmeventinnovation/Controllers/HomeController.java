package com.selectacg.daniel.ibmeventinnovation.Controllers;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.selectacg.daniel.ibmeventinnovation.R;
import com.selectacg.daniel.ibmeventinnovation.Services.ManagerLocalServicesNotifications;
import com.wikitude.samples.MainActivity;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import AsyncTask.*;

import Model.FireBase;
import Model.Settings;
import Model.Users;
import Resource.DataUser;
import Resource.PrefUtils;


/**
 * Created by SelectaCG on 18/06/15.
 */
public class HomeController extends Activity {

    Toolbar tb;
    Activity rootView;
    RelativeLayout contentPage;
    RelativeLayout progressBar;
    Bitmap bitmap;
    ImageView imagenPerfilImageView;
    Users user;

    TextView titleTextView;
    ImageView returnImageView;

    private static final int REQUEST_FINE_LOCATION=0;

    public HomeController() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        rootView = this;

        try {
            loadControllersFromView();
            loadImageProfile();
            loadArguments();
            loadEventButtons();
            loadMRUProfile();
        }catch (Exception ex){
            Log.d("Error",ex.getMessage());
        }
        try {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                    && (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    ||  checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                loadPermissions(new String[] {  Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION
                        }
                        , REQUEST_FINE_LOCATION);
            }else{
                getSettings();
            }
        }catch (Exception ex){
            Log.d("LoadPermissions","OnCreate: " + ex.getMessage());
        }
    }

    private void loadControllersFromView(){
        contentPage = (RelativeLayout) rootView.findViewById(R.id.contentPage);
        progressBar = (RelativeLayout) rootView.findViewById(R.id.contentProgressBar);
        contentPage.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        titleTextView = (TextView) findViewById(R.id.titleTextView);
        returnImageView = (ImageView) findViewById(R.id.returnImageView);
    }

    private void loadArguments(){
        titleTextView.setText(getString(R.string.title_Home));
    }

    private void loadEventButtons(){
        final Activity activity = this;
        Button tomarFotoButton = (Button) rootView.findViewById(R.id.tomarFotoButton);
        tomarFotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent MandantSelection = new Intent(activity, PrendasSimilaresController.class);
                startActivity(MandantSelection);
            }
        });

        Button pedidosButton = (Button) rootView.findViewById(R.id.pedidosButton);
        pedidosButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DownloadOrdersAsyncTask downloadOrdersAsyncTask = new DownloadOrdersAsyncTask(activity, progressBar);
                downloadOrdersAsyncTask.execute("DescargarPedidos");
            }
        });

        Button historialButton = (Button) rootView.findViewById(R.id.historialButton);
        historialButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DownloadRecordsAsyncTask downloadRecordsAsyncTask = new DownloadRecordsAsyncTask(activity, progressBar);
                downloadRecordsAsyncTask.execute("DescargarHistorial");
            }
        });

        returnImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent MandantSelection = new Intent(activity, LogoutController.class);
                startActivity(MandantSelection);
            }
        });

        titleTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent MandantSelection = new Intent(activity, MainActivity.class);
                startActivity(MandantSelection);
            }
        });

    }

    private void loadMRUProfile(){

    }

    private void loadImageProfile(){
        user = PrefUtils.getCurrentUser(HomeController.this);;
        imagenPerfilImageView = (ImageView) findViewById(R.id.ImagenPerfilImageView);

        // fetching facebook's profile picture
        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                URL imageURL = null;
                try {
                    imageURL = new URL("https://graph.facebook.com/" + user.Id + "/picture?type=large");
                } catch (MalformedURLException e) {
                    try{
                        imageURL = new URL("https://graph.facebook.com/" + "1435767690067161" + "/picture?type=large");
                    }catch (MalformedURLException ex){
                        ex.printStackTrace();
                    }
                }
                try {
                     bitmap  = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                imagenPerfilImageView.setImageBitmap(bitmap);
            }
        }.execute();
    }

    public void getSettings(){
        try {
            FireBase myFireBase = new FireBase();
            final Activity activity = this;
            myFireBase.getUrlSettings().addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    try {
                        ArrayList<Settings> listSettings = new ArrayList<>();
                        for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                            Settings settings = postSnapshot.getValue(Settings.class);
                            listSettings.add(settings);
                        }
                        if (listSettings.size() > 0) {
                            DataUser dataUser = DataUser.newInstance();
                            dataUser.settings = listSettings.get(0);

                            Intent wifiManagerService = new Intent(activity, ManagerLocalServicesNotifications.class);
                            activity.startService(wifiManagerService);
                        }else{
                            Settings settingsDefault = new Settings(6000);
                            DataUser dataUser = DataUser.newInstance();
                            dataUser.settings = settingsDefault;
                            Intent wifiManagerService = new Intent(activity, ManagerLocalServicesNotifications.class);
                            activity.startService(wifiManagerService);
                        }
                    } catch (Exception ex) {
                        Log.d("LoadEvents", ex.getMessage());
                    }
                }

                @Override
                public void onCancelled(FirebaseError error) {
                    Log.d("Details: ", error.getDetails());
                }
            });
        }catch (Exception ex){
            Log.d("EventDay","getSettings:" + ex.getLocalizedMessage());
        }
    }

    private void loadPermissions(String[] perm,int requestCode) {
        try {
            ActivityCompat.requestPermissions(this, perm, requestCode);
        }catch (Exception ex){
            Log.d("LoadPermissions","Request: " + ex.getMessage());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // granted
                    getSettings();
                }
                else{
                    // no granted
                }
                return;
            }

        }

    }
}

