package com.selectacg.daniel.ibmeventinnovation.Controllers;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.selectacg.daniel.ibmeventinnovation.R;

import java.util.ArrayList;

import Adapters.ShopsFilterAdapter;
import Model.Shops;
import Resource.DataUser;
import Resource.Fonts;

/**
 * Created by Daniel on 24/07/16.
 */
public class TiendasControllers extends Activity {


    EditText filterEditText;
    ImageView mapStoreImageView;
    ListView tiendasDisponibleListView;
    ArrayList<Shops> TiendasList;
    ShopsFilterAdapter adapter;
    RelativeLayout contentProgressBar;

    TextView titleTextView;
    ImageView returnImageView;

    public TiendasControllers(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            setContentView(R.layout.tiendas_disponibles);

            loadControllersFromView();
            loadArguments();
            loadEvents();
            loadFontAndAnimations();
        }catch (Exception ex){
            Log.d("LoadEventDay", ex.getMessage());
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        loadFontAndAnimations();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadFontAndAnimations();
    }

    private void loadControllersFromView(){
        try {
            // Inflate the layout for this fragment
            /*filterEditText = (EditText) findViewById(R.id.filterEditText);*/
            tiendasDisponibleListView = (ListView) findViewById(R.id.tiendasDisponibleListView);
            contentProgressBar = (RelativeLayout) findViewById(R.id.contentProgressBar);
            contentProgressBar.setVisibility(View.INVISIBLE);
            titleTextView = (TextView) findViewById(R.id.titleTextView);
            returnImageView = (ImageView) findViewById(R.id.returnImageView);
            mapStoreImageView = (ImageView) findViewById(R.id.mapStoreImageView);
        }catch (Exception ex){
            Log.d("EventDay", "LoadControllersFromView: " + ex.getMessage());
        }

    }

    private void loadFontAndAnimations(){
        try{
            Typeface helveticaNeueLight = Fonts.obtenerFontHelveticaNeueLight(this);
            Typeface helveticaMedium = Fonts.obtenerFontHelveticaNeueMedium(this);

            Animation translateBottomTop = AnimationUtils.loadAnimation(this, R.anim.translate_from_bottom_to_top);
            translateBottomTop.setDuration(Fonts.timeAnimations());

            titleTextView.setTypeface(helveticaMedium);
            translateBottomTop.reset();
            titleTextView.startAnimation(translateBottomTop);

            translateBottomTop.reset();
            returnImageView.startAnimation(translateBottomTop);

            /*filterEditText.setTypeface(helveticaNeueLight);
            translateBottomTop.reset();
            filterEditText.startAnimation(translateBottomTop);*/
        }catch (Exception ex){
            Log.d("LoadFontAndAnimations", "EventDayController:" + ex.getLocalizedMessage());
        }
    }

    private void loadArguments(){
        try {
            titleTextView.setText(getString(R.string.title_Shops_avaiables_list));
            TiendasList = new ArrayList<>();
            DataUser dataUser = DataUser.newInstance();
            TiendasList = dataUser.ShopsList;
            adapter = new ShopsFilterAdapter(this, TiendasList);
            //Carga del adapter que contiene los itinerarios pendientes
            tiendasDisponibleListView.setAdapter(adapter);

        }catch (Exception ex){
            Log.d("PedidosController","LoadArguments: " + ex.getMessage());
        }
    }

    private void loadEvents() {
        try {
            /*filterEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    adapter.getFilter().filter(s);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });*/

            tiendasDisponibleListView.setTextFilterEnabled(true);
            final Activity activity = this;
            tiendasDisponibleListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Shops currentEvent = (Shops) adapter.getItem(position);
                    //En esta sesion guardamos el ultimo item seleccionado para ser impreso
                    Intent MandantSelection = new Intent(activity, DescriptionController.class);
                }
            });
        } catch (Exception ex) {
        }

        final Activity current = this;
        returnImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}

