package com.selectacg.daniel.ibmeventinnovation.Controllers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.selectacg.daniel.ibmeventinnovation.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import Model.Routers;

/**
 * Created by Daniel on 19/06/16.
 */
public class WifiManager extends Activity implements View.OnClickListener
{
    android.net.wifi.WifiManager wifi;
    ListView lv;
    TextView textStatus;
    Button buttonScan;
    int size = 0;
    List<android.net.wifi.ScanResult> results;

    String ITEM_KEY = "Key";

    ArrayList<HashMap<String, String>> arraylist = new ArrayList<HashMap<String, String>>();
    ArrayList<Routers> routersAvailables = new ArrayList<>();

    SimpleAdapter adapter;

    /* Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wifi);

        textStatus = (TextView) findViewById(R.id.textStatus);
        buttonScan = (Button) findViewById(R.id.buttonScan);
        buttonScan.setOnClickListener(this);
        lv = (ListView)findViewById(R.id.list);

        wifi = (android.net.wifi.WifiManager) getSystemService(Context.WIFI_SERVICE);
        if (wifi.isWifiEnabled() == false)
        {
            Toast.makeText(getApplicationContext(), "wifi is disabled..making it enabled", Toast.LENGTH_LONG).show();
            wifi.setWifiEnabled(true);
        }
        this.adapter = new SimpleAdapter(WifiManager.this, arraylist, R.layout.row, new String[] { ITEM_KEY }, new int[] { R.id.list_value });
        lv.setAdapter(this.adapter);

        registerReceiver(new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context c, Intent intent)
            {
                results = wifi.getScanResults();
                size = results.size();
            }
        }, new IntentFilter(android.net.wifi.WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
    }

    public void onClick(View view)
    {
        arraylist.clear();
        wifi.startScan();

        Toast.makeText(this, "Scanning...." + size, Toast.LENGTH_SHORT).show();
        try
        {
            size = size - 1;
            while (size >= 0)
            {
                HashMap<String, String> item = new HashMap<String, String>();
                //Dato requerido para calcular el wifi con mayor nivel de conexion
                Integer levelWifi = results.get(size).level;
                String nameSSDI = results.get(size).SSID;
                //long timeStamp = results.get(0).timestamp;

                item.put(ITEM_KEY, "Name: " + results.get(size).SSID
                              + "\nLevel: " + results.get(size).level
                );

                Routers currentRouter = new Routers(results.get(size).SSID, results.get(size).level, "no","no");
                routersAvailables.add(currentRouter);

                arraylist.add(item);
                size--;

                Collections.sort(routersAvailables, new Comparator<Routers>() {
                    @Override
                    public int compare(Routers lhs, Routers rhs) {
                        return lhs.Level.compareTo(rhs.Level);
                    }
                });

                adapter.notifyDataSetChanged();
            }
        }
        catch (Exception e)
        { }
    }
}
