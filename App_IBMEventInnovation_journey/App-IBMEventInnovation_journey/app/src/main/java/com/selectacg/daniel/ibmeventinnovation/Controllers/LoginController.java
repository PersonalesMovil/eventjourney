package com.selectacg.daniel.ibmeventinnovation.Controllers;

/**
 * Created by Daniel on 19/06/16.
 */
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.selectacg.daniel.ibmeventinnovation.R;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import android.view.animation.AnimationUtils;

import org.json.JSONObject;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import AsyncTask.AutenticateAsyncTask;
import AsyncTask.DownloadEventsAsyncTask;
import AsyncTask.DownloadRoutersAsyncTask;
import AsyncTask.SaveUserAsyncTask;
import Model.Users;
import Resource.DataUser;
import Resource.Fonts;
import Resource.MetodosUtiles;
import Resource.PrefUtils;

public class LoginController extends Activity {

    private CallbackManager callbackManager;
    private LoginButton loginFacebookButton;
    private TwitterLoginButton loginTwitterLoginButton;
    private Button loginButton;

    private EditText emailEditText;
    private EditText passwordEditText;

    private ImageView btonLoginFaceBook;
    private ImageView btonLoginTwitter;
    private ProgressDialog progressDialog;
    LinearLayout linearEmailLinearLayout;
    LinearLayout linearPasswordLinearLayout;

    Users user;
    RelativeLayout contentProgressBar;
    Callback<TwitterSession> callbackTwiiter;
    FacebookCallback<LoginResult> mCallBackFb;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login);
        loadControllersFromView();
        loadEvents();
        loadData();
        loadFontAndAnimations();
    }

    private void loadControllersFromView(){
        try {
            contentProgressBar = (RelativeLayout) findViewById(R.id.contentProgressBar);
            contentProgressBar.setVisibility(View.INVISIBLE);
            loginButton = (Button) findViewById(R.id.loginButton);
            emailEditText = (EditText) findViewById(R.id.emailEditText);
            linearEmailLinearLayout = (LinearLayout) findViewById(R.id.linearEmailLinearLayout);
            linearPasswordLinearLayout = (LinearLayout) findViewById(R.id.linearPasswordLinearLayout);
            passwordEditText = (EditText) findViewById(R.id.passwordEditText);
            loginTwitterLoginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);
            btonLoginTwitter = (ImageView) findViewById(R.id.btnLoginTwitter);
        }catch (Exception ex){
            Log.d("LoginController","LoadController:" + ex.getLocalizedMessage());
        }

    }

    private void loadFontAndAnimations(){
        try{
            Typeface helveticaNeueLight = Fonts.obtenerFontHelveticaNeueLight(this);
            Typeface helveticaMedium = Fonts.obtenerFontHelveticaNeueMedium(this);

            Animation transladar = AnimationUtils.loadAnimation(this, R.anim.translate_from_bottom_to_top);
            transladar.setDuration(Fonts.timeAnimations());

            ImageView logoImageView = (ImageView) findViewById(R.id.logoImageView);
            transladar.reset();
            logoImageView.startAnimation(transladar);

            TextView titleEventInnovationTextView = (TextView) findViewById(R.id.titleEventInnovationTextView);
            titleEventInnovationTextView.setTypeface(helveticaMedium);

            transladar.reset();
            titleEventInnovationTextView.startAnimation(transladar);

            TextView titleMiamiEventTextView = (TextView) findViewById(R.id.titleMiamiEventTextView);
            titleMiamiEventTextView.setTypeface(helveticaMedium);

            transladar.reset();
            titleMiamiEventTextView.startAnimation(transladar);

            Animation ampliar = AnimationUtils.loadAnimation(this, R.anim.ampliar);
            transladar.setDuration(Fonts.timeAnimations());

            emailEditText.setTypeface(helveticaNeueLight);
            ampliar.reset();
            emailEditText.startAnimation(ampliar);

            passwordEditText.setTypeface(helveticaNeueLight);
            ampliar.reset();
            passwordEditText.startAnimation(ampliar);

            loginButton.setTypeface(helveticaNeueLight);
            ampliar.reset();
            loginButton.startAnimation(ampliar);

            ampliar.reset();
            btonLoginFaceBook.startAnimation(ampliar);
            ampliar.reset();
            loginFacebookButton.startAnimation(ampliar);

            ampliar.reset();
            btonLoginTwitter.startAnimation(ampliar);
            ampliar.reset();
            loginTwitterLoginButton.startAnimation(ampliar);

            ampliar.reset();
            linearEmailLinearLayout.startAnimation(ampliar);

            ampliar.reset();
            linearPasswordLinearLayout.startAnimation(ampliar);

        }catch (Exception ex){
            Log.d("LoadFontAndAnimations", "LoginController:" + ex.getLocalizedMessage());
        }
    }

    private void loadEvents(){
        emailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        final Activity activity = this;
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(emailEditText.getText().toString().trim().equalsIgnoreCase("")
                        && passwordEditText.getText().toString().trim().equalsIgnoreCase("")){
                    Intent MandantSelection = new Intent(activity, RegistroController.class);
                    MandantSelection.putExtra("Email", "");
                    MandantSelection.putExtra("Password","");
                    activity.startActivity(MandantSelection);
                }
                else if(emailEditText.getText().toString().trim().equalsIgnoreCase("")){
                    Toast.makeText(activity, "Por favor ingrese un E-mail valido", Toast.LENGTH_LONG).show();
                    emailEditText.requestFocus();
                }
                else if(passwordEditText.getText().toString().trim().equalsIgnoreCase("")){
                    Toast.makeText(activity, "Ingrese una contraseña valida", Toast.LENGTH_LONG).show();
                    passwordEditText.requestFocus();
                }
                else{
                    Pattern mPattern = Pattern.compile("^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$");
                    Matcher matcher = mPattern.matcher(emailEditText.getText().toString());
                    if(!matcher.find()) {
                        emailEditText.requestFocus();
                        Toast.makeText(activity, "E-mail Invalido", Toast.LENGTH_SHORT).show();
                    }else {
                        user = Users.newInstance();
                        user.Email = emailEditText.getText().toString();
                        user.Hour_Connection = MetodosUtiles.date_format("yyy-MM-dd HH:mm");
                        user.Id = passwordEditText.getText().toString();
                        PrefUtils.setCurrentUser(user, LoginController.this);
                        AutenticateAsyncTask autenticateAsyncTask = new AutenticateAsyncTask(activity, contentProgressBar);
                        autenticateAsyncTask.execute(user);
                    }
                }
            }
        });


        callbackTwiiter = new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls
                TwitterSession session = Twitter.getSessionManager().getActiveSession();
                TwitterAuthToken authToken = session.getAuthToken();

                user = Users.newInstance();
                user.Id = authToken.token;
                user.Name = session.getUserName();
                user.Last_Name = Long.toString(session.getUserId());
                user.Email = session.getAuthToken().secret;

                PrefUtils.setCurrentUser(user,LoginController.this);

                DataUser dataUser = DataUser.newInstance();
                dataUser.userConnect = user;
                dataUser.loginManual = false;
                dataUser.loginWithFacebook = false;
                dataUser.loginwithTwitter = true;

                SaveUserAsyncTask saveUserAsyncTask = new SaveUserAsyncTask(activity, contentProgressBar);
                saveUserAsyncTask.execute(user);

                Intent MandantSelection = new Intent(activity, HomeController.class);
                startActivity(MandantSelection);

            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure
                Log.d("FailureTwitter", exception.getMessage());

            }
        };
        btonLoginTwitter.setEnabled(false);
        btonLoginTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = new ProgressDialog(LoginController.this);
                progressDialog.setMessage("Loading...");
                progressDialog.show();

                loginTwitterLoginButton.setPressed(true);

                loginTwitterLoginButton.invalidate();

                loginTwitterLoginButton.setCallback(callbackTwiiter);

                loginTwitterLoginButton.performClick();

                loginTwitterLoginButton.setPressed(false);

                loginTwitterLoginButton.invalidate();

                loginTwitterLoginButton.getCallback();

                progressDialog.dismiss();
            }
        });

        mCallBackFb = new FacebookCallback<LoginResult>()  {
            @Override
            public void onSuccess(LoginResult loginResult) {

                progressDialog.dismiss();

                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {

                                Log.e("response: ", response + "");
                                try {
                                    user = Users.newInstance();
                                    user.Id = object.getString("id").toString();
                                    user.Email = object.getString("email").toString();
                                    user.Name = object.getString("name").toString();
                                    user.Gender = object.getString("gender").toString();
                                    PrefUtils.setCurrentUser(user,LoginController.this);

                                    DataUser dataUser = DataUser.newInstance();
                                    dataUser.userConnect = user;
                                    dataUser.loginManual = false;
                                    dataUser.loginWithFacebook = true;
                                    dataUser.loginwithTwitter = false;

                                    SaveUserAsyncTask saveUserAsyncTask = new SaveUserAsyncTask(activity, contentProgressBar);
                                    saveUserAsyncTask.execute(user);

                                    Intent MandantSelection = new Intent(activity, HomeController.class);
                                    startActivity(MandantSelection);
                                }catch (Exception e){
                                    Log.e("Error",e.getMessage());
                                }
                                Toast.makeText(LoginController.this,"welcome " + user.Name,Toast.LENGTH_LONG).show();
                            }

                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                progressDialog.dismiss();
            }

            @Override
            public void onError(FacebookException e) {
                progressDialog.dismiss();
            }
        };
    }

    private  void loadData(){
        final Activity activity = this;
        user = PrefUtils.getCurrentUser(LoginController.this);

        if(user != null) {
            if (user.Email != null && user.Name != null) {
                DataUser dataUser = DataUser.newInstance();
                dataUser.userConnect = user;
                dataUser.loginManual = false;
                dataUser.loginWithFacebook = false;
                dataUser.loginwithTwitter = false;

                SaveUserAsyncTask saveUserAsyncTask = new SaveUserAsyncTask(activity, contentProgressBar);
                saveUserAsyncTask.execute(user);

                Intent MandantSelection = new Intent(this, HomeController.class);
                startActivity(MandantSelection);

            } else {
                PrefUtils.clearCurrentUser(LoginController.this);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        callbackManager=CallbackManager.Factory.create();

        loginFacebookButton = (LoginButton)findViewById(R.id.loginFacebookbutton);

        loginFacebookButton.setReadPermissions("public_profile", "email","user_friends");

        btonLoginFaceBook = (ImageView) findViewById(R.id.btnLoginFacebook);
        //btonLoginFaceBook.setEnabled(false);
        btonLoginFaceBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog = new ProgressDialog(LoginController.this);
                progressDialog.setMessage("Loading...");
                progressDialog.show();

                loginFacebookButton.performClick();

                loginFacebookButton.setPressed(true);

                loginFacebookButton.invalidate();

                loginFacebookButton.registerCallback(callbackManager, mCallBackFb);

                loginFacebookButton.setPressed(false);

                loginFacebookButton.invalidate();

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        loginTwitterLoginButton.onActivityResult(requestCode, resultCode, data);
    }
}