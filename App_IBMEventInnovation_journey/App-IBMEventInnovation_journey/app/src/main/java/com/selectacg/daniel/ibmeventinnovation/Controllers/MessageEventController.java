package com.selectacg.daniel.ibmeventinnovation.Controllers;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.selectacg.daniel.ibmeventinnovation.R;

import AsyncTask.DownloadEventsAsyncTask;
import AsyncTask.DownloadRoutersAsyncTask;
import AsyncTask.SaveUserAsyncTask;
import Model.Users;
import Resource.DataUser;

/**
 * Created by Daniel on 21/06/16.
 */

public class MessageEventController extends Activity {

    private String TAG  = this.getClass().getSimpleName();

    public MessageEventController() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mensaje_activity);
        try {
            loadControllersFromView();
            loadEventButton();
            loadData();
        }catch (Exception ex){
            Log.d("loadData:","Error " + ex.getMessage());
        }
    }

    private void loadControllersFromView(){
        try {

        }catch (Exception ex){
            Log.d(TAG, "loadControllersFromView: ");
        }
    }

    private void loadEventButton(){

    }

    private void loadData(){

    }


}
