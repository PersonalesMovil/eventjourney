package com.selectacg.daniel.ibmeventinnovation.Controllers;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.selectacg.daniel.ibmeventinnovation.R;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import Model.Users;
import Resource.DataUser;
import Resource.Fonts;
import Resource.PrefUtils;

/**
 * Created by Daniel on 19/06/16.
 */


public class LogoutController extends Activity {

    private TextView btnLogout;
    private Users user;
    private ImageView profileImage;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);
        user = PrefUtils.getCurrentUser(LogoutController.this);
        loadControllersFromView();
        loadEvents();
        loadFontAndAnimations();

        DataUser dataUser = DataUser.newInstance();
        if(dataUser.loginwithTwitter){
            btnLogout.setText("Cerrar sesión de Twitter");
        }else if(!dataUser.loginWithFacebook){
            btnLogout.setText("Cerrar Sesión");
        }

    }

    private void loadControllersFromView(){
        try {
            profileImage = (ImageView) findViewById(R.id.profileImage);
            btnLogout = (TextView) findViewById(R.id.btnLogout);
        }catch (Exception ex){
            Log.d("LogoutController","LoadController:" + ex.getLocalizedMessage());
        }

    }

    private void loadFontAndAnimations(){
        try{
            Typeface typeLetterCustom = Fonts.obtenerFontHelveticaNeueLight(this);

            Animation transladar = AnimationUtils.loadAnimation(this, R.anim.translate_from_bottom_to_top);
            transladar.setDuration(Fonts.timeAnimations());

            ImageView image = (ImageView) findViewById(R.id.logoImageView);
            transladar.reset();
            image.startAnimation(transladar);

            TextView titleEventInnovationTextView = (TextView) findViewById(R.id.titleEventInnovationTextView);
            titleEventInnovationTextView.setTypeface(typeLetterCustom);

            transladar.reset();
            titleEventInnovationTextView.startAnimation(transladar);

            TextView titleMiamiEventTextView = (TextView) findViewById(R.id.titleMiamiEventTextView);
            titleMiamiEventTextView.setTypeface(typeLetterCustom);
            transladar.reset();
            titleMiamiEventTextView.startAnimation(transladar);

            btnLogout.setTypeface(typeLetterCustom);
            transladar.reset();
            btnLogout.startAnimation(transladar);

            transladar.reset();
            profileImage.startAnimation(transladar);
        }catch (Exception ex){
            Log.d("LoadFontAndAnimations", "LogoutController:" + ex.getLocalizedMessage());
        }
    }

    private void loadEvents(){
        // fetching facebook's profile picture
        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                URL imageURL = null;
                try {
                    imageURL = new URL("https://graph.facebook.com/" + user.Id + "/picture?type=large");
                } catch (MalformedURLException e) {
                    try{
                        imageURL = new URL("https://graph.facebook.com/" + "1435767690067161" + "/picture?type=large");
                    }catch (MalformedURLException ex){
                        ex.printStackTrace();
                    }
                }
                try {
                    bitmap  = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                profileImage.setImageBitmap(bitmap);
            }
        }.execute();

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrefUtils.clearCurrentUser(LogoutController.this);
                // We can logout from facebook by calling following method
                LoginManager.getInstance().logOut();
                Intent i= new Intent(LogoutController.this,LoginController.class);
                startActivity(i);
                finish();
            }
        });
    }
}