package com.selectacg.daniel.ibmeventinnovation.Controllers;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.selectacg.daniel.ibmeventinnovation.R;

import AsyncTask.DownloadEventsAsyncTask;
import AsyncTask.DownloadRoutersAsyncTask;
import AsyncTask.SaveUserAsyncTask;
import Model.Users;
import Resource.DataUser;
import Resource.Fonts;
import Resource.PrefUtils;

public class RegistroController extends Activity {

    String TAG = this.getClass().getSimpleName();

    EditText nameEditText;
    EditText lastNameEditText;
    EditText emailEditText;
    EditText passwordEditText;
    EditText companyEditText;
    Button nextButton;
    Button gobackButton;
    RadioButton MaleRadioButton;
    RadioButton FemenineRadioButton;
    RelativeLayout contentProgressHUD;
    ImageView logoImageView;


    public RegistroController() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro_usuario);
        try {
            loadControllersFromView();
            loadEventButton();
            loadData();
            loadFontAndAnimations();
        }catch (Exception ex){
            Log.d("loadData:","Error " + ex.getMessage());
        }
    }

    private void loadControllersFromView(){
        try {
            this.nextButton = (Button) findViewById(R.id.nextButton);
            this.nameEditText = (EditText) findViewById(R.id.nameEditText);
            this.lastNameEditText = (EditText) findViewById(R.id.lastNameEditText);
            this.passwordEditText = (EditText) findViewById(R.id.passwordEditText);
            this.emailEditText = (EditText) findViewById(R.id.emailEditText);
            this.companyEditText = (EditText) findViewById(R.id.companyEditText);
            this.MaleRadioButton = (RadioButton) findViewById(R.id.MaleRadioButton);
            this.FemenineRadioButton = (RadioButton) findViewById(R.id.FemenineRadioButton);
            this.logoImageView = (ImageView) findViewById(R.id.logoImageView);
            gobackButton = (Button) findViewById(R.id.gobackButton);
            this.contentProgressHUD = (RelativeLayout) findViewById(R.id.contentProgressBar);
            this.contentProgressHUD.setVisibility(View.INVISIBLE);
        }catch (Exception ex){
            Log.d(TAG, "loadControllersFromView: ");
        }
    }

    private void loadFontAndAnimations(){
        try{
            Typeface helveticaNeueLight = Fonts.obtenerFontHelveticaNeueLight(this);
            Typeface helveticaMedium = Fonts.obtenerFontHelveticaNeueMedium(this);

            Animation translateBottomToTop = AnimationUtils.loadAnimation(this, R.anim.translate_from_bottom_to_top);
            translateBottomToTop.setDuration(Fonts.timeAnimations());

            TextView titleEventInnovationTextView = (TextView) findViewById(R.id.titleEventInnovationTextView);
            titleEventInnovationTextView.setTypeface(helveticaMedium);

            translateBottomToTop.reset();
            titleEventInnovationTextView.startAnimation(translateBottomToTop);

            TextView titleMiamiEventTextView = (TextView) findViewById(R.id.titleMiamiEventTextView);
            titleMiamiEventTextView.setTypeface(helveticaNeueLight);

            translateBottomToTop.reset();
            titleMiamiEventTextView.startAnimation(translateBottomToTop);

            emailEditText.setTypeface(helveticaNeueLight);
            translateBottomToTop.reset();
            emailEditText.startAnimation(translateBottomToTop);

            passwordEditText.setTypeface(helveticaNeueLight);
            translateBottomToTop.reset();
            passwordEditText.startAnimation(translateBottomToTop);

            nameEditText.setTypeface(helveticaNeueLight);
            translateBottomToTop.reset();
            nameEditText.startAnimation(translateBottomToTop);

            lastNameEditText.setTypeface(helveticaNeueLight);
            translateBottomToTop.reset();
            lastNameEditText.startAnimation(translateBottomToTop);

            companyEditText.setTypeface(helveticaNeueLight);
            translateBottomToTop.reset();
            companyEditText.startAnimation(translateBottomToTop);

            FemenineRadioButton.setTypeface(helveticaNeueLight);
            translateBottomToTop.reset();
            FemenineRadioButton.startAnimation(translateBottomToTop);

            MaleRadioButton.setTypeface(helveticaNeueLight);
            translateBottomToTop.reset();
            MaleRadioButton.startAnimation(translateBottomToTop);

            translateBottomToTop.reset();
            logoImageView.startAnimation(translateBottomToTop);

            nextButton.setTypeface(helveticaNeueLight);
            translateBottomToTop.reset();
            nextButton.startAnimation(translateBottomToTop);

            gobackButton.setTypeface(helveticaNeueLight);
            translateBottomToTop.reset();
            gobackButton.startAnimation(translateBottomToTop);
        }catch (Exception ex){
            Log.d("LoadFontAndAnimations", "LoginController:" + ex.getLocalizedMessage());
        }
    }

    private void loadEventButton(){

        gobackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        final  Activity activity = this;
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (nameEditText.getText().toString().trim().equalsIgnoreCase("")) {
                        nameEditText.requestFocus();
                    } else if (lastNameEditText.getText().toString().trim().equalsIgnoreCase("")) {
                        lastNameEditText.requestFocus();
                    } else if (passwordEditText.getText().toString().trim().equalsIgnoreCase("")) {
                        passwordEditText.requestFocus();
                    } else if (emailEditText.getText().toString().trim().equalsIgnoreCase("")) {
                        emailEditText.requestFocus();
                    } else {
                        Users users = Users.newInstance();
                        users.Id = passwordEditText.getText().toString();
                        users.Email = emailEditText.getText().toString();
                        users.Company = companyEditText.getText().toString();
                        users.Name = nameEditText.getText().toString();
                        users.Last_Name = lastNameEditText.getText().toString();
                        try {
                            users.Gender = MaleRadioButton.isChecked() ? "Male" : "Femenine";
                        }catch (Exception ex){
                            users.Gender = "Male";
                        }
                        PrefUtils.setCurrentUser(users, RegistroController.this);

                        DataUser dataUser = DataUser.newInstance();
                        dataUser.userConnect = users;

                        DownloadEventsAsyncTask downloadEventsAsyncTask = new DownloadEventsAsyncTask(activity, contentProgressHUD);
                        downloadEventsAsyncTask.execute("");

                        DownloadRoutersAsyncTask downloadRoutersAsyncTask = new DownloadRoutersAsyncTask(activity, contentProgressHUD);
                        downloadRoutersAsyncTask.execute("");

                        SaveUserAsyncTask saveUserAsyncTask = new SaveUserAsyncTask(activity, contentProgressHUD);
                        saveUserAsyncTask.execute(users);
                    }
                }catch (Exception ex){
                    Log.d("OnClick", "RegistroController" + ex.getMessage());
                }
            }
        });


    }

    private void loadData(){
        try {
            Bundle arguments = getIntent().getExtras();
            if (arguments != null) {
                emailEditText.setText(arguments.getString("Email"));
                passwordEditText.setText(arguments.getString("Password"));
            }
        }catch (Exception e){
            Log.d(TAG,"LoadData: " + e.getMessage());
        }
    }
}
