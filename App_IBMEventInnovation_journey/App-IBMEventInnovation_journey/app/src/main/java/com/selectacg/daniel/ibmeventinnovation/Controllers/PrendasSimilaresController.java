package com.selectacg.daniel.ibmeventinnovation.Controllers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import Adapters.ItemsFilterAdapter;
import AsyncTask.BitmapWorkerTask;
import AsyncTask.DownloadItemsAsyncTask;
import Model.Items;
import Model.Photos;

import com.selectacg.daniel.ibmeventinnovation.R;

import Enum.TypePhoto;
import Model.Records;
import Resource.DataUser;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by SelectaCG on 14/07/15.
 */

public class PrendasSimilaresController extends Activity {

    // Activity result Key for camera
    static final int REQUEST_TAKE_PHOTO = 11111;
    private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;
    ArrayList<Photos> listZTBILL_FOTOS;
    ArrayList<CameraActivity> listActivityImages;
    ArrayList<CameraActivity> listActivityVideo;
    ArrayList<ImageView> listImagesImageView;
    ArrayList<VideoView> listVideosVideoView;
    ArrayList<LinearLayout> listlinealLinearLayout;
    HorizontalScrollView tblcontenedor;

    private int countImageCurrent;
    private int countVideoCurrent;

    private Boolean isCam;
    RelativeLayout contentPage;
    RelativeLayout contentProgressBar;
    LinearLayout linealNuevo;
    Activity rootView;

    String imageFileNameCurrent;
    ListView prendasSimilaresListView;
    ItemsFilterAdapter adapter;

    TextView titleTextView;
    ImageView returnImageView;

    ImageView addFotoImageView;
    ImageView editFotoImageView;
    ImageView deleteFotoImageView;

    public PrendasSimilaresController() {
        super();
        listActivityImages = new ArrayList<>();
        listActivityVideo = new ArrayList<>();
        listImagesImageView = new ArrayList<>();
        listVideosVideoView = new ArrayList<>();
        listlinealLinearLayout = new ArrayList<>();
        listZTBILL_FOTOS = new ArrayList<>();
        countImageCurrent = 0;
        countVideoCurrent = 0;
        isCam = false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prendas_similares);
        rootView = this;
        try {
            loadControllersFromView();
            loadArguments();
            loadEvents();
        }catch (Exception ex){
            Log.d("ErrorImage", "OnCreateView:" + ex.getMessage());
        }
    }

    private void loadControllersFromView(){
        contentPage = (RelativeLayout) rootView.findViewById(R.id.contentPage);
        contentProgressBar = (RelativeLayout) rootView.findViewById(R.id.contentProgressBar);
        contentPage.setVisibility(View.VISIBLE);
        contentProgressBar.setVisibility(View.INVISIBLE);
        prendasSimilaresListView = (ListView) rootView.findViewById(R.id.prendasSimilaresListView);
        tblcontenedor = (HorizontalScrollView) rootView.findViewById(R.id.contenedorImagesVideosTableLayout);

        addFotoImageView = (ImageView) rootView.findViewById(R.id.addFotoImageView);
        editFotoImageView = (ImageView) rootView.findViewById(R.id.editFotoImageView);
        deleteFotoImageView = (ImageView) rootView.findViewById(R.id.deleteFotoImageView);

        titleTextView = (TextView) findViewById(R.id.titleTextView);
        returnImageView = (ImageView) findViewById(R.id.returnImageView);
    }

    private void loadArguments(){
        try {
            titleTextView.setText("Prendas Similares");
            DataUser user = DataUser.newInstance();
            ArrayList<Items> similarItems =  user.ItemsList;
            listZTBILL_FOTOS = user.photosSavedList;
            if (similarItems == null){
                capturePhoto();
            }else{
                adapter = new ItemsFilterAdapter(this, similarItems, contentProgressBar);
                //Carga del adapter que contiene los itinerarios pendientes
                prendasSimilaresListView.setAdapter(adapter);
            }
            if (listZTBILL_FOTOS.size() > 0) {
                loadImagesSaved();
            }
        }catch (Exception ex){
            Log.d("Error", ex.getMessage());
        }
    }

    private void loadEvents(){
        prendasSimilaresListView.setTextFilterEnabled(true);
        final Activity activity = this;
        prendasSimilaresListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Items current = (Items) adapter.getItem(position);

            }
        });

        returnImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addFotoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                capturePhoto();
            }
        });

        editFotoImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(activity, "Proximamente", Toast.LENGTH_SHORT).show();
                    }
                });

        deleteFotoImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, "Proximamente", Toast.LENGTH_SHORT).show();
            }
        });
    }

    protected void dispatchTakeVideoIntent() {
        try{
            isCam = false;
            // checkea si existe una camara en el dispositivo
            Context context = this;
            PackageManager packageManager = context.getPackageManager();
            if(packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA) == false){
                Toast.makeText(this, "El dispositivo no tiene camara", Toast.LENGTH_SHORT)
                        .show();
                return;
            }
            // Si la camara existe, genera el proceso
            Intent takePictureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

            CameraActivity activity = new CameraActivity();
            // if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
            // Create the File where the photo should go.
            // If you don't do this, you may get a crash in some devices.
            File videoFile = null;
            try {
                videoFile = createVideoFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast toast = Toast.makeText(this, "No se pudo guardar el video correctamente...", Toast.LENGTH_SHORT);
                toast.show();
            }
            // Continue only if the File was successfully created
            if (videoFile != null) {
                Uri fileUri = Uri.fromFile(videoFile);
                activity.setCapturedImageURI(fileUri);
                activity.setCurrentPhotoPath(fileUri.getPath());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        activity.getCapturedImageURI());
                listActivityVideo.add(activity);
                countVideoCurrent++;
                startActivityForResult(takePictureIntent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);
            }
            //}
        }catch (Exception ex){
            String strError = ex.getMessage();
            System.out.println(strError);
        }
    }

    protected void dispatchTakePictureIntent() {
        try{
            isCam = true;
            // checkea si existe una camara en el dispositivo
            Context context = this;
            PackageManager packageManager = context.getPackageManager();
            if(packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA) == false){
                Toast.makeText(this, "El dispositivo no tiene camara", Toast.LENGTH_SHORT)
                        .show();
                return;
            }
            // Si la camara existe, genera el proceso
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            CameraActivity activity = new CameraActivity();
            //if (takePictureIntent != null) {
                // Create the File where the photo should go.
                // If you don't do this, you may get a crash in some devices.
            File photoFile = null;
            try {
                String timeStamp = new SimpleDateFormat(getString(R.string.format_date_image)).format(new Date());
                String name = timeStamp + "_Name";
                photoFile = createImageFile(name);
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri fileUri = Uri.fromFile(photoFile);
                    activity.setCapturedImageURI(fileUri);
                    activity.setCurrentPhotoPath(fileUri.getPath());
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            activity.getCapturedImageURI());
                    listActivityImages.add(activity);
                    countImageCurrent++;
                    try {
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                    }catch (Exception ex){
                        Log.d("action cam", ex.getMessage());
                    }
                }

            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast toast = Toast.makeText(this, "No se pudo guardar la foto correctamente", Toast.LENGTH_SHORT);
                toast.show();
            }
            //}
        }catch (Exception ex){
            Log.d("ErrorImage", "DispcherTakePhoto:" + ex.getMessage());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        try {
            if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
                try {
                    addPhotoVideoToGallery(1);
                    DownloadItemsAsyncTask downloadItemsAsyncTask = new DownloadItemsAsyncTask(this, contentProgressBar);
                    downloadItemsAsyncTask.execute("DownloadSimilarItems");
                }catch (Exception ex){
                    Log.d("OnActivitResultCamera","Error: " + ex.getMessage());
                }
                try {
                    loadImageRequest(false, null);
                }catch (Exception ex){
                    Log.d("ErrorImage","Error: " + ex.getMessage());

                }
            }else if (requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                loadVideoRequest();
            } else {
                if(!isCam) {
                    Toast.makeText(this, "No se grabo el video", Toast.LENGTH_SHORT)
                            .show();
                }else{
                    Toast.makeText(this, "No se capturo la imagen", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        }catch (Exception ex){
            Log.d("ErrorImage", "OnResultActivity:" + ex.getMessage());
        }
    }

    private void setFullImageFromFilePath(String imagePath, ImageView imageView, Boolean isLoadInit){
        try {
            BitmapWorkerTask task = new BitmapWorkerTask(imageView, imagePath);
            task.execute(0);
            String nombreFichero = imageFileNameCurrent;
            Photos ztbill_fotos = new Photos();
            ztbill_fotos.setNombreFichero(nombreFichero);
            ztbill_fotos.currentPATH = imagePath;
            if (!containsPhotoInList(listZTBILL_FOTOS,ztbill_fotos)) {
                listZTBILL_FOTOS.add(ztbill_fotos);
                DataUser user = DataUser.newInstance();
                user.photosSavedList = listZTBILL_FOTOS;
            }
        }catch (Exception ex){
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void setFullVideoFromFilePath(String imagePath, VideoView videoView){
        videoView.setVideoPath(imagePath);
    }

    protected File createImageFile(String name) throws IOException {
        try {
            // Create an image file name
            imageFileNameCurrent = name;
            File storageDir = Environment.getExternalStoragePublicDirectory(TypePhoto.FOLDER_TO_SAVE);
            if (storageDir.exists()) {
                File image = File.createTempFile(imageFileNameCurrent, ".jpg", storageDir);
                return image;
            }else{
                storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
                if (storageDir.exists()) {
                    File image = File.createTempFile(imageFileNameCurrent, ".jpg", storageDir);
                    return image;
                }else{
                    storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                    if (storageDir.exists()) {
                        File image = File.createTempFile(imageFileNameCurrent, ".jpg", storageDir);
                        return image;
                    }else{
                        Toast.makeText(this, "Debe crear una carpeta llamada" + TypePhoto.FOLDER_TO_SAVE, Toast.LENGTH_SHORT).show();
                    }
                }
            }
            //Save a file: path for use with ACTION_VIEW intents
            //listActivityImages.get(countImageCurrent).setCurrentPhotoPath("file:" + image.getAbsolutePath());
            return null;
        }catch ( Exception ex){
            Log.d("Error","createImageFile:" + ex.getMessage());
        }
        return null;
    }

    protected File createVideoFile() throws IOException {
        File image;
        try {
            // Create an image file name
            String timeStamp = new SimpleDateFormat(getString(R.string.format_date_image)).format(new Date());
            String imageFileName = "VID_" + timeStamp + "_";
            File storageDir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".mp4",         /* suffix */
                    storageDir      /* directory */
            );

            // Save a file: path for use with ACTION_VIEW intents
            //CameraActivity activity = (CameraActivity)this;
            //activity.setCurrentPhotoPath("file:" + image.getAbsolutePath());
        }catch (Exception ex){
            Log.d("load video",ex.getMessage());
        }
        return null;
    }

    protected void addPhotoVideoToGallery(int type) {
        try {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            //CameraActivity activity = (CameraActivity)this;
            CameraActivity activity;
            if(type == 1) {
                 activity = listActivityImages.get(countImageCurrent - 1);
            }else{
                activity = listActivityVideo.get(countVideoCurrent - 1);
            }
            File f = new File(activity.getCurrentPhotoPath());
            Uri contentUri = Uri.fromFile(f);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);
        }catch (Exception ex){
            Log.d("Error", "AddPhotoToGallery:" + ex.getMessage());
        }
    }

    private Boolean capturePhoto(){
        try {
            long spaceMinToSave = 10485760;
            long spaceAvailable = Environment.getDataDirectory().getFreeSpace();
            Log.i("Espacio disponible cel", Long.toString(spaceAvailable));
            long spaceAvailableSD = Environment.getExternalStorageDirectory().getFreeSpace();
            Log.i("Espacio disponible sd", Long.toString(spaceAvailableSD));
            long freeSpaceRootDirectory = Environment.getRootDirectory().getFreeSpace();
            String availableSD = Environment.getExternalStorageState();
            Boolean availableSDbit = false;
            if (availableSD.equalsIgnoreCase(Environment.MEDIA_MOUNTED)) {
                availableSDbit = true;
            }
            if ((availableSDbit && spaceAvailableSD > spaceMinToSave) || (spaceAvailable > spaceMinToSave)) {
                if (listImagesImageView.size() < 3) {
                    dispatchTakePictureIntent();
                    return true;
                } else {
                    //Mensaje para indicar que a superado el limite de imagenes y videos
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(getString(R.string.msj_count_image_permited))
                            .setTitle(getString(R.string.msj_warning));
                    builder.setPositiveButton(getString(R.string.ctl_OK), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    return false;
                }
            } else {
                //Mensaje para indicar que no hay espacio disponible en la memoria
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.msj_not_space_available))
                        .setTitle(getString(R.string.msj_warning));
                builder.setPositiveButton(getString(R.string.ctl_OK), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                return false;
            }
        }catch (Exception ex){
            Log.d("save photo", ex.getMessage());
            return false;
        }
    }

    private void captureVideo(){
        dispatchTakeVideoIntent();
    }

    private void loadVideoRequest(){
        try {
            addPhotoVideoToGallery(2);
            // Show the full sized image.
            LinearLayout linealNuevo;
            RelativeLayout.LayoutParams regla;
            if (listlinealLinearLayout.size() == 0) {

                linealNuevo = new LinearLayout(this);
                regla = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 500);
                linealNuevo.setOrientation(LinearLayout.HORIZONTAL);

                switch (listlinealLinearLayout.size()) {
                    case 0:
                        linealNuevo.setId(R.id.imageVideoLineal1);
                        //regla.addRule(RelativeLayout.BELOW, R.id.contenedorImagesVideosTableLayout);
                        regla.addRule(RelativeLayout.CENTER_HORIZONTAL);
                        regla.addRule(RelativeLayout.CENTER_VERTICAL);
                        //regla.addRule(RelativeLayout.CENTER_IN_PARENT);
                        break;
                    case 1:
                        linealNuevo.setId(R.id.imageVideoLineal2);
                        regla.addRule(RelativeLayout.BELOW, R.id.imageVideoLineal1);
                        break;
                    case 2:
                        linealNuevo.setId(R.id.imageVideoLineal3);
                        regla.addRule(RelativeLayout.BELOW, R.id.imageVideoLineal2);
                        break;
                    case 3:
                        linealNuevo.setId(R.id.imageVideoLineal4);
                        regla.addRule(RelativeLayout.BELOW, R.id.imageVideoLineal3);
                        break;
                    case 4:
                        linealNuevo.setId(R.id.imageVideoLineal5);
                        regla.addRule(RelativeLayout.BELOW, R.id.imageVideoLineal4);
                        break;
                    case 5:
                        linealNuevo.setId(R.id.imageVideoLineal6);
                        regla.addRule(RelativeLayout.BELOW, R.id.imageVideoLineal5);
                        break;
                }
                linealNuevo.setLayoutParams(regla);
                tblcontenedor.addView(linealNuevo);
            } else {
                linealNuevo = listlinealLinearLayout.get(0);
            }

            VideoView videoCurrent = new VideoView(this.getApplicationContext());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 400);
            videoCurrent.setLayoutParams(layoutParams);
            videoCurrent.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (listVideosVideoView.contains(v)) {
                        int pos = listVideosVideoView.indexOf(v);
                        listVideosVideoView.remove(pos);
                        listActivityVideo.remove(pos);
                        listZTBILL_FOTOS.remove(pos);
                        countVideoCurrent--;
                        v.destroyDrawingCache();
                        v.setVisibility(View.INVISIBLE);
                        Toast.makeText(rootView, getString(R.string.msj_deleted_video_selected), Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        Toast.makeText(rootView, getString(R.string.msj_cannot_deleted_video_selected), Toast.LENGTH_SHORT)
                                .show();
                    }
                    return true;
                }
            });

            videoCurrent.getLayoutParams().height = 500;
            videoCurrent.getLayoutParams().width = 500;
            linealNuevo.addView(videoCurrent);
            listVideosVideoView.add(videoCurrent);
            setFullVideoFromFilePath(listActivityVideo.get(countVideoCurrent - 1).getCurrentPhotoPath(), listVideosVideoView.get(countVideoCurrent - 1));
        }catch (Exception ex){
            Toast.makeText(this, "error:" + ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void loadImageRequest(Boolean isLoadInit, Photos current){
        try {
            LinearLayout.LayoutParams regla;
            if (listlinealLinearLayout.size() == 0) {
                linealNuevo = new LinearLayout(this);
                regla = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
                regla.gravity = Gravity.CENTER;
                linealNuevo.setOrientation(LinearLayout.HORIZONTAL);

                switch (listlinealLinearLayout.size()) {
                    case 0:
                        linealNuevo.setId(R.id.imageVideoLineal1);
                        break;
                }
                //Esta funcion alinea la imagen al centro de la pantalla
                //regla.addRule(RelativeLayout.CENTER_VERTICAL);
                linealNuevo.setLayoutParams(regla);
                listlinealLinearLayout.add(linealNuevo);
                //Se añade el linealLayout al tableLayout
                tblcontenedor.addView(linealNuevo);
            } else {
                linealNuevo = listlinealLinearLayout.get(0);
            }

            try {
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                int width = metrics.widthPixels; // ancho absoluto en pixels
                int height = metrics.heightPixels;

                ImageView imageCurrent = new ImageView(this);

                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width/2,height/3);
                layoutParams.gravity = Gravity.CENTER;
                layoutParams.weight = 1;
                layoutParams.setMargins(50,0,0,0);
                imageCurrent.setLayoutParams(layoutParams);
                imageCurrent.setBackgroundColor(getResources().getColor(R.color.mi_azul));
                imageCurrent.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        try {
                            if (listImagesImageView.contains(v)) {
                                int pos = listImagesImageView.indexOf(v);
                                linealNuevo.removeView(v);
                                listImagesImageView.remove(pos);
                                listActivityImages.remove(pos);
                                listZTBILL_FOTOS.remove(pos);
                                countImageCurrent--;
                                Toast.makeText(rootView, getString(R.string.msj_deleted_image_selected), Toast.LENGTH_SHORT)
                                        .show();
                            } else {
                                Toast.makeText(rootView, getString(R.string.msj_cannot_deleted_image_selected), Toast.LENGTH_SHORT)
                                        .show();
                            }
                        }catch (Exception ex){
                            Log.d("Error delete image", ex.getMessage());
                        }
                        return true;
                    }
                });

                //Se añaden la imagen al linearLayout
                linealNuevo.addView(imageCurrent);
                listImagesImageView.add(imageCurrent);
                String path;
                if(current != null) {
                    path = current.currentPATH;
                }else{
                    path = listActivityImages.get(countImageCurrent - 1).getCurrentPhotoPath();
                }
                setFullImageFromFilePath(path, listImagesImageView.get(countImageCurrent - 1), isLoadInit);

            }catch (Exception ex){
                Log.d("save image", ex.getMessage());
            }
        }catch (Exception ex){
            Log.d("Error load image", ex.getMessage());
        }
    }

    private void loadImagesSaved() throws IOException {
        try {
            for (Photos current : listZTBILL_FOTOS) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                CameraActivity activity = new CameraActivity();
                File photoFile = createImageFile(current.getNombreFichero());
                if (photoFile != null) {
                    Uri fileUri = Uri.fromFile(photoFile);
                    activity.setCapturedImageURI(fileUri);
                    activity.setCurrentPhotoPath(fileUri.getPath());
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            activity.getCapturedImageURI());
                    listActivityImages.add(activity);
                    countImageCurrent++;
                    try {
                        loadImageRequest(true, current);
                    } catch (Exception ex) {
                        Log.d("load cam", ex.getMessage());
                    }
                }
            }
        }catch (Exception ex){
            Log.d("load cam",ex.getMessage());
        }
    }

    private Boolean containsPhotoInList(ArrayList<Photos> listFotos, Photos photo){
        for (Photos current: listFotos){
               if (current.getNombreFichero().equalsIgnoreCase(photo.getNombreFichero()) &&
                       current.currentPATH.equalsIgnoreCase(photo.currentPATH)){
                   return true;
               }
        }
        return false;
    }
}

