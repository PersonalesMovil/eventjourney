package com.selectacg.daniel.ibmeventinnovation.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.selectacg.daniel.ibmeventinnovation.Controllers.DescriptionController;
import com.selectacg.daniel.ibmeventinnovation.Controllers.PedidosController;
import com.selectacg.daniel.ibmeventinnovation.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import AsyncTask.AudiosAsyncTask;
import Model.FireBase;
import Model.Movements;
import Model.NotificationsEvents;
import Model.Orders;
import Model.Routers;
import Model.Users;
import Resource.DataUser;
import Resource.MetodosUtiles;

import static android.provider.Settings.System.AIRPLANE_MODE_ON;

public class ManagerLocalServicesNotifications extends Service {

    Timer tempTimer = new Timer();
    long updateInterval = 60000;
    IBinder mBinder = new MyBinder();

    ArrayList<Routers> routersAvailables = new ArrayList<>();
    ArrayList<Orders> ordersPending = new ArrayList<>();

    WifiManager wifi;

    int size = 0;
    List<ScanResult> results;

    Boolean segundaVez = false;

    public static final String inputFormat = "HH:mm";

    SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.US);


    private static final int REQUEST_FINE_LOCATION=0;

    public ManagerLocalServicesNotifications() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            DataUser dataUser = DataUser.newInstance();
            if (dataUser != null) {
                updateInterval = dataUser.settings.Time_Active * 1000;
            }
        }catch (Exception ex){
            Log.d("UpdateInterval","Error: " + ex.getMessage());
        }finally {
            Log.d("Update Interval: ", "Interval: " + updateInterval);
        }

        wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        if (wifi.isWifiEnabled() == false){
            wifi.setWifiEnabled(true);
            Log.d("Wifi","Actived");
        }

        registerReceiver(new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context c, Intent intent)
            {
                results = wifi.getScanResults();
                size = results.size();
            }
        }, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));


        getAllRouters();
        getAllOrders();
        networkWifiUpdateState();
    }

    private void networkWifiUpdateState(){
        tempTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try
                {
                    Log.d("Servicio","Activo tiempo: " + updateInterval);
                    wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                    if (wifi.isWifiEnabled() == false){
                        wifi.setWifiEnabled(true);
                    }

                    routersAvailables = new ArrayList<Routers>();
                    wifi.startScan();
                    size = size - 1;
                    while (size >= 0)
                    {
                        try {
                            Routers currentRouter = new Routers(results.get(size).SSID, results.get(size).level, "", "");
                            routersAvailables.add(currentRouter);
                            size--;
                        }catch (Exception ex){
                            size--;
                        }
                    }

                    try {
                        if (routersAvailables.size() > 0) {
                            DataUser dataUser = DataUser.newInstance();
                            if (dataUser != null) {
                                if (dataUser.routersList != null) {
                                    Routers routerMoreLevel = getRoutersMoreLevel(routersAvailables, dataUser.routersList);
                                    Log.d("----","-------------------------------------------");
                                    Log.d("Mas Cercano", "Nombre:" + routerMoreLevel.Name);
                                    Log.d("Distancia", "Level: " + routerMoreLevel.Level);
                                    Log.d("----","-------------------------------------------");
                                    if (routerMoreLevel != null) {
                                        if(dataUser.lastRouterRegister == null){
                                            if(routerMoreLevel.Level >= -30) {
                                                dataUser.lastRouterRegister = routerMoreLevel;
                                                dataUser.lastRouterRegister.notified = false;
                                                NotificationsEvents notificationsEvents = new NotificationsEvents();
                                                notificationsEvents.Email = dataUser.userConnect.Email;
                                                notificationsEvents.Hour_notified = MetodosUtiles.date_format(getString(R.string.format_date));
                                                notificationsEvents.isNotified = "Yes";
                                                notificationsEvents.Name_Router = routerMoreLevel.Name;
                                                Boolean isWelcome = true;
                                                routerMoreLevel.Title = "Ingresando a la Tienda";
                                                routerMoreLevel.Speaker = "";
                                                routerMoreLevel.Image_Url = " ";
                                                routerMoreLevel.Message = "Bienvenido a su tienda favorita";
                                                routerMoreLevel.Date = "En este momento";
                                                routerMoreLevel.Hour = "Ahora";
                                                validateNotificationsUser(notificationsEvents, routerMoreLevel, dataUser.userConnect, isWelcome);
                                            }
                                        }else if (routerMoreLevel.Name.trim().equalsIgnoreCase(dataUser.lastRouterRegister.Name.trim())){
                                            if(routerMoreLevel.Level < -30 && !dataUser.lastRouterRegister.notified){
                                                dataUser.lastRouterRegister = routerMoreLevel;
                                                dataUser.lastRouterRegister.notified = true;
                                                NotificationsEvents notificationsEvents = new NotificationsEvents();
                                                notificationsEvents.Email = dataUser.userConnect.Email;
                                                notificationsEvents.Hour_notified = MetodosUtiles.date_format(getString(R.string.format_date));
                                                notificationsEvents.isNotified = "Yes";
                                                notificationsEvents.Name_Router = routerMoreLevel.Name;
                                                Boolean isWelcome = false;
                                                routerMoreLevel.Title = "Salida de la tienda";
                                                routerMoreLevel.Speaker = "";
                                                routerMoreLevel.Image_Url = " ";
                                                routerMoreLevel.Message = "Se esta Saliendo de la tienda, Vuelva pronto";
                                                routerMoreLevel.Date = "En este momento";
                                                routerMoreLevel.Hour = "Ahora";
                                                validateNotificationsUser(notificationsEvents, routerMoreLevel, dataUser.userConnect, isWelcome);

                                            }else if(routerMoreLevel.Level >= -30 && dataUser.lastRouterRegister.notified){
                                                dataUser.lastRouterRegister = routerMoreLevel;
                                                dataUser.lastRouterRegister.notified = false;
                                                NotificationsEvents notificationsEvents = new NotificationsEvents();
                                                notificationsEvents.Email = dataUser.userConnect.Email;
                                                notificationsEvents.Hour_notified = MetodosUtiles.date_format(getString(R.string.format_date));
                                                notificationsEvents.isNotified = "Yes";
                                                notificationsEvents.Name_Router = routerMoreLevel.Name;
                                                Boolean isWelcome = true;
                                                routerMoreLevel.Title = "Ingresando a la tienda";
                                                routerMoreLevel.Speaker = "";
                                                routerMoreLevel.Image_Url = " ";
                                                routerMoreLevel.Message = "Bienvenido a su tienda Favorita";
                                                routerMoreLevel.Date = "En este momento";
                                                routerMoreLevel.Hour = "Ahora";
                                                validateNotificationsUser(notificationsEvents, routerMoreLevel, dataUser.userConnect, isWelcome);
                                            }
                                        }else{
                                            dataUser.lastRouterRegister = routerMoreLevel;
                                            dataUser.lastRouterRegister.notified = true;
                                            NotificationsEvents notificationsEvents = new NotificationsEvents();
                                            notificationsEvents.Email = dataUser.userConnect.Email;
                                            notificationsEvents.Hour_notified = MetodosUtiles.date_format(getString(R.string.format_date));
                                            notificationsEvents.isNotified = "Yes";
                                            notificationsEvents.Name_Router = routerMoreLevel.Name;
                                            Boolean isWelcome = false;
                                            routerMoreLevel.Title = "Salida de la tienda";
                                            routerMoreLevel.Speaker = "";
                                            routerMoreLevel.Image_Url = " ";
                                            routerMoreLevel.Message = "Se esta Saliendo de la tienda, Vuelva pronto";
                                            routerMoreLevel.Date = "En este momento";
                                            routerMoreLevel.Hour = "Ahora";
                                            validateNotificationsUser(notificationsEvents, routerMoreLevel, dataUser.userConnect, isWelcome);
                                        }
                                    }else{
                                        if(dataUser.lastRouterRegister != null){
                                            NotificationsEvents notificationsEvents = new NotificationsEvents();
                                            notificationsEvents.Email = dataUser.userConnect.Email;
                                            notificationsEvents.Hour_notified = MetodosUtiles.date_format(getString(R.string.format_date));
                                            notificationsEvents.isNotified = "Yes";
                                            notificationsEvents.Name_Router = routerMoreLevel.Name;
                                            Boolean isWelcome = false;
                                            routerMoreLevel.Title = "Salida de la tienda";
                                            routerMoreLevel.Speaker = "";
                                            routerMoreLevel.Image_Url = " ";
                                            routerMoreLevel.Message = "Se esta Saliendo de la tienda, Vuelva pronto";
                                            routerMoreLevel.Date = "En este momento";
                                            routerMoreLevel.Hour = "Ahora";
                                            validateNotificationsUser(notificationsEvents, routerMoreLevel, dataUser.userConnect, isWelcome);
                                            dataUser.lastRouterRegister = null;
                                        }
                                    }
                                }
                            }
                        }
                    }catch (Exception ex){
                        Log.d("ValidateRouter","Error service: " + ex.getMessage());
                    }

                    if(ordersPending.size() > 0){
                        try {
                            for (Orders current : ordersPending) {
                                try {
                                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                                    String str1 = MetodosUtiles.date_format("yyyy-MM-dd HH:mm");
                                    Date date1 = formatter.parse(str1);

                                    String str2 = current.Purchase_Date;
                                    Date date2 = formatter.parse(str2);

                                    if (date1.compareTo(date2) > 0 && !current.isNotified) {
                                        long diff = date1.getTime() - date2.getTime();
                                        long segundos = diff / 1000;
                                        long minutos = segundos / 60;
                                        if(minutos < 2) {
                                            Boolean isWelcome = true;
                                            notifyExapandable(current, R.mipmap.ic_logo_negro, isWelcome);
                                            current.isNotified = true;
                                        }
                                    }
                                } catch (ParseException e1) {
                                    e1.printStackTrace();
                                }
                            }

                        }catch (Exception ex){
                            Log.d("Order Pending","Error: " + ex.getMessage());
                        }

                    }
                    getAllRouters();
                }
                catch (Exception e)
                {
                    Log.d("ServiceLocal","Error: " + e.getMessage());
                }
            }
        }, updateInterval, updateInterval);
    }

    public Boolean saveMovements(Movements movements){

        try {
            FireBase myFireBase = new FireBase();

            myFireBase.getUrlMovements().push().setValue(movements, new Firebase.CompletionListener() {
                @Override
                public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                    if(firebaseError != null){
                        Log.e("errorSaveMovements", firebaseError.getMessage());
                    }else{
                        Log.d("Movements save suces:","Firebase");
                    }
                }
            });
        }catch (Exception ex){
            Log.e("RegisterUser",ex.getMessage());
        }
        return true;
    }

    public Boolean updateMovements(Map<String, Object> movements){
        try {
            FireBase myFireBase = new FireBase();

            myFireBase.getUrlMovements().updateChildren(movements, new Firebase.CompletionListener() {
                @Override
                public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                    if(firebaseError != null){
                        Log.e("error: ", "update Movements " + firebaseError.getMessage());
                    }else{
                        Log.d("Movements:","update success Firebase");
                    }
                }
            });
        }catch (Exception ex){
            Log.e("RegisterUser",ex.getMessage());
        }
        return true;
    }

    public void getMovementsForUser(final Users users, final Routers router){
        FireBase myFireBase = new FireBase();
        myFireBase.getUrlMovements().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                try {
                    ArrayList<Movements> listMovements = new ArrayList<>();
                    Boolean isEqual = false;
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        try {
                            String key = postSnapshot.getKey();
                            Movements movements = postSnapshot.getValue(Movements.class);
                            movements.key = key;
                            if (movements.Email.equalsIgnoreCase(users.Email)) {
                                listMovements.add(movements);
                            }
                        }catch (Exception ex){

                        }
                    }

                    if (listMovements.size() > 0) {
                        Log.d("Movimientos","Cantidad: " + listMovements.size());
                        for (Movements current : listMovements){
                               if(current.Name_Router.equalsIgnoreCase(router.Name)
                                       && current.Hour_Disable.trim().equalsIgnoreCase("")){
                                   isEqual = true;
                               }
                        }
                        if(!isEqual){
                            Movements lastMovement = null;
                            for (Movements current : listMovements){
                                if(current.Hour_Disable.trim().equalsIgnoreCase("")){
                                    lastMovement = current;
                                }
                            }

                            if(lastMovement != null){
                                lastMovement.Hour_Disable = MetodosUtiles.date_format(getString(R.string.format_date));
                                Map<String, Object> movementsToUpdateKey = new HashMap<String, Object>();
                                Map<String, Object> movementsToUpdateDetails = new HashMap<String, Object>();
                                movementsToUpdateDetails.put("Email",lastMovement.Email);
                                movementsToUpdateDetails.put("Name_Router",lastMovement.Name_Router);
                                movementsToUpdateDetails.put("Id",lastMovement.Id);
                                movementsToUpdateDetails.put("Hour_Disable",lastMovement.Hour_Disable);
                                movementsToUpdateDetails.put("Hour_Available",lastMovement.Hour_Available);
                                movementsToUpdateDetails.put("Level",lastMovement.Level);
                                movementsToUpdateKey.put(lastMovement.key,movementsToUpdateDetails);
                                updateMovements(movementsToUpdateKey);
                                Log.d("Movimiento","Actualizado");
                            }

                            Movements movements = new Movements();
                            movements.Id = "0";
                            movements.Hour_Available = MetodosUtiles.date_format(getString(R.string.format_date));
                            movements.Hour_Disable = " ";
                            movements.Name_Router = router.Name;
                            movements.Level = router.Level;
                            movements.Email = users.Email;
                            saveMovements(movements);
                            Log.d("Movimiento","Guardado");
                            if (router.IsMessage.equalsIgnoreCase("YES")) {
                                Log.d("notificacion","Activa");
                                NotificationsEvents notificationsEvents = new NotificationsEvents();
                                notificationsEvents.Email = users.Email;
                                notificationsEvents.Hour_notified = MetodosUtiles.date_format(getString(R.string.format_date));
                                notificationsEvents.isNotified = "Yes";
                                notificationsEvents.Name_Router = router.Name;
                                Boolean isWelcome = true;
                                validateNotificationsUser(notificationsEvents, router, users, isWelcome);
                            }
                        }else{
                            if (router.IsMessage.equalsIgnoreCase("YES")) {
                                Log.d("Notificacion","Activa");
                                NotificationsEvents notificationsEvents = new NotificationsEvents();
                                notificationsEvents.Email = users.Email;
                                notificationsEvents.Hour_notified = MetodosUtiles.date_format(getString(R.string.format_date));
                                notificationsEvents.isNotified = "Yes";
                                notificationsEvents.Name_Router = router.Name;
                                Boolean isWelcome = true;
                                validateNotificationsUser(notificationsEvents, router, users, isWelcome);
                            }
                        }
                    }else {
                        Movements movements = new Movements();
                        movements.Id = "0";
                        movements.Hour_Available = MetodosUtiles.date_format(getString(R.string.format_date));
                        movements.Hour_Disable = " ";
                        movements.Name_Router = router.Name;
                        movements.Level = router.Level;
                        movements.Email = users.Email;
                        saveMovements(movements);
                        Log.d("Movimiento","Guardado");
                        if (router.IsMessage.equalsIgnoreCase("YES")) {
                            Log.d("Notificacion","Activa");
                            NotificationsEvents notificationsEvents = new NotificationsEvents();
                            notificationsEvents.Email = users.Email;
                            notificationsEvents.Hour_notified = MetodosUtiles.date_format(getString(R.string.format_date));
                            notificationsEvents.isNotified = "no";
                            notificationsEvents.Name_Router = router.Name;
                            Boolean isWelcome = true;
                            validateNotificationsUser(notificationsEvents, router, users, isWelcome);
                        }
                    }
                }catch (Exception ex) {
                    Log.d("MovementsForUser","Error: " + ex.getMessage());
                }
            }
            @Override public void onCancelled(FirebaseError error) {
                System.out.println("Details: " + error.getDetails());
            }
        });
    }

    public void validateNotificationsUser(final NotificationsEvents notificationsEvents, final Routers routerWithEvent, final Users dataUser,final Boolean isWelcome){
        try{
            AudiosAsyncTask audiosAsyncTask = new AudiosAsyncTask(getBaseContext());
            audiosAsyncTask.execute(isWelcome);
        }catch (Exception ex){
            Log.d("Mensaje","Error al intentar reproducir audio: " + ex.getMessage());
        }

        //Guardar Notificacion
        /*FireBase myFireBase = new FireBase();
        try {
            myFireBase.getUrlNotificationsEvents().push().setValue(notificationsEvents, new Firebase.CompletionListener() {
                @Override
                public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                    if (firebaseError != null) {
                        Log.d("Notificacion", "Error: " + firebaseError.getMessage());
                    } else {
                        Log.d("Notifications", "Succesful");
                    }
                }
            });
        }catch (Exception ex){
            Log.d("Error","No se pudo guardar la notificacion en firebase " + ex.getMessage());
        }
        try{
            Log.d("Notificacion","Valida");
            notifyExapandable(routerWithEvent,
                    R.mipmap.ic_logo_negro,
                    notificationsEvents, isWelcome);
            Log.d("Notificacion","Enviada");
        }catch (Exception ex){
            Log.d("ValidateNotifications","Error: " + ex.getMessage());
        }*/
    }

    public void getAllRouters(){
        FireBase myFireBase = new FireBase();
        myFireBase.getUrlRouters().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                try {
                    ArrayList<Routers> listRouters = new ArrayList<>();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        try {
                            Routers routers = postSnapshot.getValue(Routers.class);
                            listRouters.add(routers);
                        }catch (Exception e){
                            Log.d("Router","Error al Convertir Wifi: " + e.getMessage());
                        }
                    }
                    if (listRouters.size() > 0) {
                        Log.d("Routers","Cantidad: " + listRouters.size());
                        DataUser dataUser = DataUser.newInstance();
                        dataUser.routersList = listRouters;
                    }
                }catch (Exception ex) {
                    Log.d("getAllRouters", ex.getMessage());
                }
            }
            @Override public void onCancelled(FirebaseError error) {
                Log.d("OnCancelled: ", error.getDetails());
            }
        });
    }

    public void getAllOrders(){
        FireBase myFireBase = new FireBase();
        myFireBase.getUrlOrders().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                try {
                    ArrayList<Orders> listOrders = new ArrayList<>();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        Orders orders = postSnapshot.getValue(Orders.class);
                        listOrders.add(orders);
                    }
                    if (listOrders.size() > 0) {
                        Log.d("Routers","Cantidad: " + listOrders.size());
                        DataUser dataUser = DataUser.newInstance();
                        dataUser.ordersList = listOrders;
                        ordersPending = listOrders;
                    }
                }catch (Exception ex) {
                    Log.d("getAllRouters", ex.getMessage());
                }
            }
            @Override public void onCancelled(FirebaseError error) {
                Log.d("OnCancelled: ", error.getDetails());
            }
        });
    }

    public Routers getRoutersMoreLevel(ArrayList<Routers> listRouters, ArrayList<Routers> listRoutersSettings){
        try {
            Routers result = null;
            if (listRouters.size() > 0) {
                for (int i = 0; i < listRouters.size(); i++) {
                    for (Routers settingRouter : listRoutersSettings) {
                        if (settingRouter.Name.equalsIgnoreCase(listRouters.get(i).Name)) {
                            if (result == null) {
                                result = listRouters.get(i);
                                result.IsMessage = settingRouter.IsMessage;
                                result.Message = settingRouter.Message;
                                result.Date = settingRouter.Date;
                                result.Duration = settingRouter.Duration;
                                result.Hour = settingRouter.Hour;
                                result.Image_Url = settingRouter.Image_Url;
                                result.Speaker  = settingRouter.Speaker;
                                result.Title = settingRouter.Title;
                                result.Name = settingRouter.Name;
                            } else if (result.Level < listRouters.get(i).Level) {
                                result = listRouters.get(i);
                                result.IsMessage = settingRouter.IsMessage;
                                result.Message = settingRouter.Message;
                                result.Date = settingRouter.Date;
                                result.Duration = settingRouter.Duration;
                                result.Hour = settingRouter.Hour;
                                result.Image_Url = settingRouter.Image_Url;
                                result.Speaker  = settingRouter.Speaker;
                                result.Title = settingRouter.Title;
                                result.Name = settingRouter.Name;
                            }
                        }
                    }
                }
            }
            return result;
        }catch (Exception ex){
            Log.d("RouterMoreLevel", ex.getMessage());
            return null;
        }
    }

    static boolean isAirplaneModeOn(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        return android.provider.Settings.System.getInt(contentResolver, AIRPLANE_MODE_ON, 0) != 0;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(tempTimer != null){
            tempTimer.cancel();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class MyBinder extends Binder{

        ManagerLocalServicesNotifications getService(){
            return ManagerLocalServicesNotifications.this;
        }

    }

    public List<Routers> getWifiList(){
        return routersAvailables;
    }

    //Notificacion simple
    public void simpleNotify(int id, int iconId, String titulo, String contenido) {
        NotificationManager notifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(iconId)
                        .setLargeIcon(BitmapFactory.decodeResource(
                                getResources(),
                                R.mipmap.ic_logo_azul
                                )
                        )
                        .setContentTitle(titulo)
                        .setContentText(contenido)
                        .setColor(getResources().getColor(R.color.mi_rojo));


        // Construir la notificación y emitirla
        notifyMgr.notify(id, builder.build());
    }

    //Notificacion con evento de actividad regular
    public void regultarActivityNotify(int id, int iconId, String titulo, String contenido ) {
        NotificationManager notifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Creación del builder
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(iconId)
                        .setLargeIcon(BitmapFactory.decodeResource(
                                getResources(),
                                R.mipmap.ic_logo_azul
                                )
                        )
                        .setContentTitle(titulo)
                        .setContentText(contenido)
                        .setColor(getResources().getColor(R.color.mi_rojo));

        // Nueva instancia del intent apuntado hacia Eslabon
        Intent intent = new Intent(this, PedidosController.class);

        // Crear pila
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        // Añadir actividad padre
        stackBuilder.addParentStack(PedidosController.class);

        // Referenciar Intent para la notificación
        stackBuilder.addNextIntent(intent);

        // Obtener PendingIntent resultante de la pila
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        // Asignación del pending intent
        builder.setContentIntent(resultPendingIntent);

        // Remover notificacion al interactuar con ella
        builder.setAutoCancel(true);

        // Construir la notificación y emitirla
        notifyMgr.notify(id, builder.build());
    }

    //Grupo de notificaciones para eliminar cuando se considere necesario
    public void notifyGrouping() {
        NotificationManager notifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        String GRUPO_NOTIFICACIONES = "notificaciones_similares";
        Notification notificacion;

        // Comprobar si ya fue presionado el item
        if(!segundaVez) {
            notificacion = new NotificationCompat.Builder(this)
                    .setContentTitle("Mensaje Nuevo")
                    .setSmallIcon(R.mipmap.ic_logo_azul)
                    .setContentText("Carlos Arándano   ¿Donde estás?")
                    .setColor(getResources().getColor(R.color.mi_rojo))
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .build();

            // Activar la bandera
            segundaVez = true;

        }else {

            notificacion = new NotificationCompat.Builder(this)
                    .setContentTitle("2 mensajes nuevos")
                    .setSmallIcon(R.mipmap.ic_logo_azul)
                    .setNumber(2)
                    .setColor(getResources().getColor(R.color.mi_rojo))
                    .setStyle(
                            new NotificationCompat.InboxStyle()
                                    .addLine("Carlos Arándano   ¿Si lo viste?")
                                    .addLine("Ximena Claus   Nuevo diseño del logo")
                                    .setBigContentTitle("2 mensajes nuevos")
                    )
                    .setGroup(GRUPO_NOTIFICACIONES)
                    .setGroupSummary(true)
                    .build();
        }

        notifyMgr.notify(3, notificacion);
    }

    //Notificacion expandible
    public void notifyExapandable(Routers event, int iconId, NotificationsEvents notificationsEvents, Boolean isWelcome){
        NotificationManager notifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Intent intent = new Intent(this, DescriptionController.class);
        intent.putExtra("Title", event.Title);
        intent.putExtra("Description", event.Message);
        intent.putExtra("Hour", event.Hour);
        intent.putExtra("Speaker", event.Speaker);
        intent.putExtra("duration",event.Duration);
        intent.putExtra("Image_Url", event.Image_Url);
        intent.putExtra("isWelcome", isWelcome);
        intent.putExtra("notificationID", event.Level);

        intent.setAction(Intent.ACTION_VIEW);
        PendingIntent piDismiss = PendingIntent.getActivity(
                this,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        //PendingIntent dismissIntent = NotificationActivity.getDismissIntent(notificationId, context);

        //Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Uri alarmWelcome;
        if(isWelcome) {
            alarmWelcome = Uri.parse("android.resource: //" + getPackageName() + "/"
                    + R.raw.ste_18);
        }else{
            alarmWelcome = Uri.parse ( "android.resource: //" + getPackageName () + "/"
                    + R.raw.a_006);
        }
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(iconId)
                        .setLargeIcon(BitmapFactory.decodeResource(
                                getResources(),
                                R.mipmap.ic_logo_negro
                                )
                        )
                        .setContentTitle(event.Title)
                        .setContentText(event.Message)
                        .setColor(getResources().getColor(R.color.mi_azul))
                        .setStyle(
                                new NotificationCompat.BigTextStyle().setSummaryText(event.Message))
                        .addAction(R.drawable.ic_clothes,
                                "Detalle", piDismiss)
                        .addAction(R.drawable.ic_action_name,
                                "Listo", null)
                        .setNumber(event.Level)
                ;

        Notification notification = mBuilder.build();
        MetodosUtiles.pausarProcesos(1000);
        // Construir la notificación y emitirla
        notifyMgr.notify(event.Level, notification);
    }

    public void notifyExapandable(Orders orders, int iconId, Boolean isWelcome){
        NotificationManager notifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        Intent intent = new Intent(this, DescriptionController.class);
        intent.putExtra("Title", "Confirmacion llegada Almacen");

        intent.putExtra("Address_Store", orders.Address_Store);
        intent.putExtra("Date", orders.Purchase_Date);
        intent.putExtra("Gargament_Type",orders.Garment_Type);
        intent.putExtra("Qualification",orders.Qualification);
        intent.putExtra("isCancel",orders.isCancel);
        intent.putExtra("isConfirm",orders.isConfirm);
        intent.putExtra("Url_Photo", orders.Url_Photo);

        intent.setAction(Intent.ACTION_VIEW);
        PendingIntent piDismiss = PendingIntent.getActivity(
                this,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        //PendingIntent dismissIntent = NotificationActivity.getDismissIntent(notificationId, context);

        Uri alarmWelcome = Uri.parse ( "android.resource: //" + getPackageName () + "/"
                + R.raw.a_000);

        Uri alarm = Uri.parse ( "android.resource: //" + getPackageName () + "/"
                + R.raw.a_006);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(iconId)
                        .setLargeIcon(BitmapFactory.decodeResource(
                                getResources(),
                                R.mipmap.ic_logo_negro
                                )
                        )
                        .setContentTitle("Confirmacion Llegada")
                        .setContentText(orders.Name_Store)
                        .setColor(getResources().getColor(R.color.mi_azul))
                        .setStyle(
                                new NotificationCompat.BigTextStyle().setSummaryText(orders.Garment_Type
                                        +"\nDireccion: " + orders.Address_Store))

                        .addAction(R.drawable.ic_clothes,
                                "Detalle", piDismiss)
                        .addAction(R.drawable.ic_action_name,
                                "OK", null)
                        .setVibrate(new long[]{2000,2000,2000,2000})
                        .setLocalOnly(true)
                        .setNumber(orders.Qualification)
                ;
        Notification notification = mBuilder.build();
        // Construir la notificación y emitirla
        notifyMgr.notify(0, notification);

        try{
            AudiosAsyncTask audiosAsyncTask = new AudiosAsyncTask(getBaseContext());
            audiosAsyncTask.execute(isWelcome);
        }catch (Exception ex){
            Log.d("Mensaje","Error al intentar reproducir audio: " + ex.getMessage());
        }
    }
    //Notificacion con barra de progreso
    public void progressBarNotify(final int id, int iconId, String titulo, String contenido) {
        final NotificationManager notifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Integer noNotificaciones = 2;

        final NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(iconId)
                        .setContentTitle(titulo)
                        .setContentText(contenido).setNumber(noNotificaciones);

        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        int i;

                        // Ciclo para la simulación de progreso
                        for (i = 0; i <= 100; i += 5) {
                            // Setear 100% como medida máxima
                            builder.setProgress(100, i, false);
                            // Emitir la notificación
                            notifyMgr.notify(id, builder.build());
                            // Retardar la ejecución del hilo
                            try {
                                // Retardo de 1s
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                Log.d("Error", "Falló sleep(1000) ");
                            }
                        }

                    /*
                    ACTUALIZACIÓN DE LA NOTIFICACION
                     */

                        // Desplegar mensaje de éxito al terminar
                        builder.setContentText("Sincronización Completa")
                                // Quitar la barra de progreso
                                .setProgress(0, 0, false);
                        notifyMgr.notify(id, builder.build());
                    }
                }

        ).start();

    }

    //Notificacion de aviso
    public void notifyAviso(int id, int iconId, String titulo, String contenido ) {

        final NotificationManager notifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Integer noNotificaciones = 2;

        // Estructurar la notificación
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(iconId)
                        .setContentTitle(titulo)
                        .setContentText(contenido).setNumber(noNotificaciones);

        // Crear intent
        Intent intent = new Intent(this, PedidosController.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Crear pending intent
        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(
                this,
                0,
                intent,
                PendingIntent.FLAG_CANCEL_CURRENT);

                // Asignar intent y establecer true para notificar como aviso
                builder.setFullScreenIntent(fullScreenPendingIntent, true);

                // Construir la notificación y emitirla
                notifyMgr.notify(id, builder.build());
    }
}
