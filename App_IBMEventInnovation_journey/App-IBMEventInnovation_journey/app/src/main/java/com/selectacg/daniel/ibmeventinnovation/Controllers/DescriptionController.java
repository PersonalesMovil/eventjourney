package com.selectacg.daniel.ibmeventinnovation.Controllers;

import android.app.Activity;
import android.app.NotificationManager;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.selectacg.daniel.ibmeventinnovation.R;
import com.selectacg.daniel.ibmeventinnovation.Views.Zoom;

import AsyncTask.DownloadImageTask;
import Resource.Fonts;

/**
 * Created by Daniel on 19/06/16.
 */
public class DescriptionController extends Activity {

    Boolean isOrderActive;

    ImageView locationImageView;
    TextView titleEventTextView;
    TextView descriptionTextView;

    ImageView storeIconImageView;
    TextView titleStoreTextView;
    TextView storeTextView;

    ImageView clothingTypeIconImageView;
    TextView titleClothingTypeTextView;
    TextView clothingTypeTextView;

    ImageView dateIconImageView;
    TextView titleDateTextView;
    TextView dateTextView;

    TableRow ButtonsReservationsActive;
    Button confirmButton;
    Button cancelButton;

    RatingBar qualificationRatingBar;

    ImageView returnImageView;

    TextView titleTextView;

    ScrollView detailsScrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.descripcion_evento);
            loadControllersFromView();
            loadEvents();
            loadData();
            loadFontAndAnimations();
        }catch (Exception ex){
            Log.e("LoginLoad", ex.getLocalizedMessage());
        }
    }

    private void loadControllersFromView(){
        try {
            this.locationImageView = (ImageView) findViewById(R.id.locationImageView);
            this.descriptionTextView = (TextView) findViewById(R.id.descriptionTextView);
            this.titleEventTextView = (TextView) findViewById(R.id.titleNameStoreTextView);

            storeIconImageView = (ImageView)  findViewById(R.id.storeIconImageView);
            titleStoreTextView = (TextView) findViewById(R.id.titleAddressStoreTextView);
            storeTextView = (TextView) findViewById(R.id.storeTextView);

            clothingTypeIconImageView = (ImageView)  findViewById(R.id.clothingTypeIconImageView);
            titleClothingTypeTextView = (TextView) findViewById(R.id.titleClothingTypeTextView);
            clothingTypeTextView = (TextView) findViewById(R.id.clothingTypeTextView);

            dateIconImageView = (ImageView) findViewById(R.id.dateIconImageView);
            titleDateTextView = (TextView) findViewById(R.id.titleDateTextView);
            dateTextView = (TextView) findViewById(R.id.dateTextView);

            qualificationRatingBar = (RatingBar)  findViewById(R.id.qualificationRatingBar);

            ButtonsReservationsActive = (TableRow)  findViewById(R.id.ButtonsReservationsActive);
            this.confirmButton = (Button) findViewById(R.id.ConfirmButton);
            this.cancelButton = (Button) findViewById(R.id.CancelButton);

            titleTextView = (TextView) findViewById(R.id.titleTextView);
            returnImageView = (ImageView) findViewById(R.id.returnImageView);

            detailsScrollView = (ScrollView) findViewById(R.id.detailsScrollView);
        }catch (Exception ex){
            Log.d("DescriptionController","LoadControllerFromView error: " + ex.getMessage());
        }
    }

    private void loadFontAndAnimations(){
        try{
            Typeface helveticaNeueLight = Fonts.obtenerFontHelveticaNeueLight(this);
            Typeface helveticaMedium = Fonts.obtenerFontHelveticaNeueMedium(this);

            Animation translateBottomToTop = AnimationUtils.loadAnimation(this, R.anim.translate_from_bottom_to_top);
            translateBottomToTop.setDuration(Fonts.timeAnimations());

            Animation translateTopToBottom = AnimationUtils.loadAnimation(this, R.anim.translate_from_top_to_bottom);
            translateTopToBottom.setDuration(Fonts.timeAnimations());

            titleTextView.setTypeface(helveticaMedium);
            translateTopToBottom.reset();
            titleTextView.startAnimation(translateTopToBottom);

            translateTopToBottom.reset();
            returnImageView.startAnimation(translateTopToBottom);

            Animation scaleImage = AnimationUtils.loadAnimation(this, R.anim.ampliar);
            scaleImage.setDuration(Fonts.timeAnimations());
            scaleImage.reset();
            //locationImageView.startAnimation(scaleImage);

            titleEventTextView.setTypeface(helveticaMedium);
            translateBottomToTop.reset();
            //titleEventTextView.startAnimation(translateBottomToTop);

            descriptionTextView.setTypeface(helveticaNeueLight);
            translateBottomToTop.reset();
            //descriptionTextView.startAnimation(translateBottomToTop);

            titleStoreTextView.setTypeface(helveticaMedium);
            translateBottomToTop.reset();
           // titleStoreTextView.startAnimation(translateBottomToTop);

            storeTextView.setTypeface(helveticaNeueLight);
            translateBottomToTop.reset();
            //storeTextView.startAnimation(translateBottomToTop);

            titleClothingTypeTextView.setTypeface(helveticaMedium);
            translateBottomToTop.reset();
            //titleClothingTypeTextView.startAnimation(translateBottomToTop);
            translateBottomToTop.reset();
            //clothingTypeIconImageView.startAnimation(translateBottomToTop);

            clothingTypeTextView.setTypeface(helveticaNeueLight);
            translateBottomToTop.reset();
            //clothingTypeTextView.startAnimation(translateBottomToTop);

            titleDateTextView.setTypeface(helveticaMedium);
            //translateBottomToTop.reset();
            //titleDateTextView.startAnimation(translateBottomToTop);
            //translateBottomToTop.reset();
            //dateIconImageView.startAnimation(translateBottomToTop);
            dateTextView.setTypeface(helveticaNeueLight);
            //translateBottomToTop.reset();
            //dateTextView.startAnimation(translateBottomToTop);

            Animation transparencia = AnimationUtils.loadAnimation(this, R.anim.transparencia);
            transparencia.setDuration(Fonts.timeAnimations());
            //transparencia.reset();
            //detailsScrollView.startAnimation(transparencia);

        }catch (Exception ex){
            Log.d("LoadFontAndAnimations", "DescriptionController:" + ex.getLocalizedMessage());
        }
    }

    private void loadEvents(){

        returnImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final Activity activity = this;
        locationImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(new Zoom(activity, locationImageView));
            }
        });

        final Activity current = this;
        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(current,"Se ha confirmado la reserva", Toast.LENGTH_LONG).show();
                ButtonsReservationsActive.setVisibility(View.INVISIBLE);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Toast.makeText(current,"Se ha cancelado la reserva", Toast.LENGTH_LONG).show();
                ButtonsReservationsActive.setVisibility(View.INVISIBLE);
            }
        });


    }

    private void loadData(){
        try {
            Bundle arguments = getIntent().getExtras();
            if (arguments != null) {
                isOrderActive = arguments.getBoolean("isOrderActive");
                titleEventTextView.setText(arguments.getString("Title"));
                descriptionTextView.setText(arguments.getString("Description"));
                storeTextView.setText(arguments.getString("Date"));
                clothingTypeTextView.setText(arguments.getString("Name_Store"));
                dateTextView.setText(arguments.getString("clothing_Type"));
                qualificationRatingBar.setMax(5);
                qualificationRatingBar.setNumStars(arguments.getInt("Qualification"));
                String Image_Url = arguments.getString("url_Photo");
                if(!isOrderActive){
                    ButtonsReservationsActive.setVisibility(View.INVISIBLE);
                }
                if(Image_Url != null) {
                    if (!Image_Url.trim().equalsIgnoreCase("")) {
                        DownloadImageTask downloadImageTask = new DownloadImageTask(locationImageView);
                        downloadImageTask.execute(Image_Url);
                    }
                }
                if(arguments.getBoolean("isWelcome")){
                    titleTextView.setText("Entrando a su Tienda");

                }else{
                    titleTextView.setText("Saliendo de su Tienda");
                }
            }

            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            // Cancelamos la Notificacion que hemos comenzado
            nm.cancel(getIntent().getExtras().getInt("notificationID"));
        }catch (Exception ex){
            Log.d("DescriptionEvent", "LoadData: " + ex.getLocalizedMessage());
        }
    }
}
