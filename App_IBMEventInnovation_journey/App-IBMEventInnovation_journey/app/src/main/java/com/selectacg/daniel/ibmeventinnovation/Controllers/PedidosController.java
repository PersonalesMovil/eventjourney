package com.selectacg.daniel.ibmeventinnovation.Controllers;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.selectacg.daniel.ibmeventinnovation.R;

import java.util.ArrayList;

import Adapters.PedidosFilterAdapter;
import Model.Orders;
import Resource.DataUser;
import Resource.Fonts;

/**
 * Created by SelectaCG on 13/07/15.
 */
public class PedidosController extends Activity {

    EditText filterEditText;
    ListView misPedidosListView;
    ArrayList<Orders> PedidosList;
    PedidosFilterAdapter adapter;
    RelativeLayout contentProgressBar;
    LinearLayout toolbar;
    TextView titleTextView;
    ImageView returnImageView;

    public PedidosController(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            setContentView(R.layout.mis_pedidos);

            loadControllersFromView();
            loadArguments();
            loadEvents();
            loadFontAndAnimations();
        }catch (Exception ex){
            Log.d("LoadEventDay", ex.getMessage());
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        loadFontAndAnimations();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadFontAndAnimations();
    }

    private void loadControllersFromView(){
        try {
            // Inflate the layout for this fragment
            filterEditText = (EditText) findViewById(R.id.filterEditText);
            misPedidosListView = (ListView) findViewById(R.id.misPedidosListView);
            contentProgressBar = (RelativeLayout) findViewById(R.id.contentProgressBar);
            contentProgressBar.setVisibility(View.INVISIBLE);
            toolbar = (LinearLayout) findViewById(R.id.toolbarTop);
            titleTextView = (TextView) findViewById(R.id.titleTextView);
            returnImageView = (ImageView) findViewById(R.id.returnImageView);
        }catch (Exception ex){
            Log.d("EventDay", "LoadControllersFromView: " + ex.getMessage());
        }

    }

    private void loadFontAndAnimations(){
        try{
            Typeface helveticaNeueLight = Fonts.obtenerFontHelveticaNeueLight(this);
            Typeface helveticaMedium = Fonts.obtenerFontHelveticaNeueMedium(this);

            Animation translateBottomTop = AnimationUtils.loadAnimation(this, R.anim.translate_from_bottom_to_top);
            translateBottomTop.setDuration(Fonts.timeAnimations());

            titleTextView.setTypeface(helveticaMedium);
            translateBottomTop.reset();
            titleTextView.startAnimation(translateBottomTop);

            filterEditText.setTypeface(helveticaNeueLight);
            translateBottomTop.reset();
            filterEditText.startAnimation(translateBottomTop);

            translateBottomTop.reset();
            returnImageView.startAnimation(translateBottomTop);

        }catch (Exception ex){
            Log.d("LoadFontAndAnimations", "EventDayController:" + ex.getLocalizedMessage());
        }
    }

    private void loadArguments(){
        try {
            PedidosList = new ArrayList<>();
            DataUser dataUser = DataUser.newInstance();
            ArrayList<Orders> allOrdes = dataUser.ordersList;
            for (Orders current: allOrdes){
               if(!contains(PedidosList, current.email)){
                   PedidosList.add(current);
               }
            }
            adapter = new PedidosFilterAdapter(this, PedidosList, contentProgressBar);
            //Carga del adapter que contiene los itinerarios pendientes
            misPedidosListView.setAdapter(adapter);

            titleTextView.setText(getString(R.string.title_Orders_list));
        }catch (Exception ex){
            Log.d("PedidosController","LoadArguments: " + ex.getMessage());
        }
    }

    private void loadEvents(){
        try {
            filterEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    adapter.getFilter().filter(s);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            misPedidosListView.setTextFilterEnabled(true);
            final Activity activity = this;
            misPedidosListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Orders currentEvent = (Orders) adapter.getItem(position);
                    //En esta sesion guardamos el ultimo item seleccionado para ser impreso
                    Intent MandantSelection = new Intent(activity, DescriptionController.class);
                }
            });
        }catch (Exception ex){
        }

        final Activity current = this;
        returnImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });
    }

    private Boolean contains(ArrayList<Orders> events, String email){
        for (Orders current: events){
            if(current.email == email){
                return true;
            }
        }
        return false;
    }


}
