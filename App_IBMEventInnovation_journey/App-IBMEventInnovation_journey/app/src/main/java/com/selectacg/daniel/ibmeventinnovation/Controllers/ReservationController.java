package com.selectacg.daniel.ibmeventinnovation.Controllers;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TimePicker;

import com.selectacg.daniel.ibmeventinnovation.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import AsyncTask.SaveOrderAsyncTask;
import Model.Orders;
import Resource.DataUser;
import Resource.DateTimeCustom;

/**
 * Created by SelectaCG on 21/07/15.
 */
public class ReservationController extends android.app.DialogFragment {

    Toolbar tb;
    View rootView;
    EditText dateEditText;
    EditText hourEditText;
    DatePickerDialog datePickerDialog;
    TimePickerDialog hourPickerDialog;
    DateTimeCustom date;
    Resource.Time currentTimeSel;

    SimpleDateFormat dateFormat;

    RelativeLayout contenedorInfo;
    RelativeLayout progressBar;

    Button saveButton;
    Button cancelButton;

    public ReservationController() {
        date = new DateTimeCustom();
    }

    public static ReservationController newInstance(Bundle arguments) {
        ReservationController f = new ReservationController();
        if(arguments != null){
            f.setArguments(arguments);
        }
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.reservas, container, false);
        dateFormat = new SimpleDateFormat(getString(R.string.format_date), Locale.US);
        getDialog().setTitle("Reservar");
        getDialog().setCancelable(false);
        loadInfoControllers();
        loadControllers(rootView);
        loadEventEditText();
        loadEventButtons(rootView);
        loadKeyEvents();
        return rootView;
    }

    private void loadKeyEvents(){
        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
    }

    private void loadInfoControllers() {
        Calendar newCalendar = Calendar.getInstance();

        datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener()  {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                date.year = year;
                date.month = monthOfYear;
                date.day = dayOfMonth;
                dateEditText.setText(dateFormat.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        hourPickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                currentTimeSel = new Resource.Time();
                currentTimeSel.setHour(hourOfDay);
                currentTimeSel.setMinute(minute);
                hourEditText.setText(currentTimeSel.getHour() + ":" + currentTimeSel.getMinute());
            }
        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), false);
    }


    private void loadControllers(View rootView){
        contenedorInfo = (RelativeLayout) rootView.findViewById(R.id.contenedorInfo);
        progressBar = (RelativeLayout) rootView.findViewById(R.id.contentProgressBar);
        contenedorInfo.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        dateEditText = (EditText)rootView.findViewById(R.id.dateReservationEditText);
        hourEditText = (EditText)rootView.findViewById(R.id.timeReservationEditText);
    }

    private void loadEventButtons(View rootView){
        saveButton = (Button)rootView.findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateSave();
            }
        });

        cancelButton = (Button) rootView.findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               dismiss();
            }
        });
    }

    private void clearInfo(){
        try {
            dateEditText.setText("");
            hourEditText.setText("");
        }catch (Exception ex){
            Log.e("Clear gauges hourly", ex.getMessage());
        }
    }

    private void validateSave() {
        try {
           if (dateEditText.getText().toString().trim().equals("")) {dateEditText.requestFocus();}
           else if(hourEditText.getText().toString().trim().equals("")){hourEditText.requestFocus();}
            else {
                try {
                    DataUser user = DataUser.newInstance();
                    Orders newOrder = new Orders();
                    newOrder.Name_Store = "Ejemplo Reserva";
                    newOrder.email = user.userConnect.Email;
                    newOrder.isCancel = false;
                    newOrder.isConfirm = false;
                    newOrder.Address_Store = "calle 43 # 31 -21";
                    newOrder.Garment_Type = "Camisilla";
                    newOrder.Qualification = 4;
                    newOrder.Purchase_Date = dateEditText.getText().toString() + " " + hourEditText.getText().toString();
                    newOrder.Url_Photo = " ";
                    SaveOrderAsyncTask saveOrderAsyncTask = new SaveOrderAsyncTask(getActivity(), this, progressBar);
                    saveOrderAsyncTask.execute(newOrder);
                }catch (Exception ex){
                    Log.d("ValidateReserva","Error: " + ex.getMessage());
                }
                saveButton.setText(getString(R.string.ctl_continue));
            }
        }catch (Exception ex){
            Log.e("Save Gauges hourly", ex.getMessage());
        }
    }

    private void loadEventEditText() {
        dateEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(dateEditText.getWindowToken(), 0);
                if (hasFocus) {
                    datePickerDialog.show();
                }
            }
        });

        hourEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(hourEditText.getWindowToken(), 0);
                if (hasFocus) {
                    hourPickerDialog.show();
                }
            }
        });
    }
}
