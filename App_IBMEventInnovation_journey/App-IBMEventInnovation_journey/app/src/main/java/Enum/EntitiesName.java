package Enum;

/**
 * Created by Daniel on 16/06/16.
 */
public class EntitiesName {

    public static String Root = "DataIBMEvent";
    public static String Users = "Users";
    public static String Events = "Events";
    public static  String Routers = "Routers";
    public static  String Settings = "Settings";
    public static  String Movements = "Movements";
    public static  String NotificationsEvents = "NotificationsEvents";
    public static  String Items = "Items";
    public static  String Orders = "Orders";
    public static  String Records = "Records";
    public static  String Shops = "Shops";

}
