package Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.selectacg.daniel.ibmeventinnovation.Controllers.ReservationController;
import com.selectacg.daniel.ibmeventinnovation.R;

import java.util.ArrayList;
import java.util.List;

import Model.Shops;
import Resource.Fonts;

/**
 * Created by Daniel on 24/07/16.
 */
public class ShopsFilterAdapter extends ArrayAdapter<Shops> implements Filterable {

    private ArrayList<Integer> mSelection = new ArrayList<Integer>();
    private SearchSuppliesFilter searchSuppliesFilter;
    private ArrayList<Shops> detailFilterList;
    private Activity activity;

    TextView titleNameStoreTextView;

    TextView titleClothingAvailableTextView;
    TextView titleQualificationTextView;
    TextView titleAddressStoreTextView;

    TextView clothingAvailableTextView;
    RatingBar QualificationRatingBar;
    TextView addressStoreTextView;

    ImageView clothingAvailableIconImageView;
    ImageView qualificationIconImageView;
    ImageView addressStoreIconImageView;

    Button requestAsesorieButton;
    Button requestReservationButton;

    LinearLayout contenedorListaDia;

    public ShopsFilterAdapter(Activity context, ArrayList<Shops> items) {
        super(context, R.layout.tiendas_disponible_detalle, items);
        this.activity = context;
        this.detailFilterList = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            // This a new view we inflate the new layout
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.tiendas_disponible_detalle, parent, false);
        }

        contenedorListaDia = (LinearLayout) convertView.findViewById(R.id.contenedorListaDia);

        titleNameStoreTextView = (TextView) convertView.findViewById(R.id.titleNameStoreTextView);

        titleClothingAvailableTextView = (TextView) convertView.findViewById(R.id.titleclothingAvailablesTextView);
        titleQualificationTextView = (TextView) convertView.findViewById(R.id.titleQualificationTextView);
        titleAddressStoreTextView = (TextView) convertView.findViewById(R.id.titleAddressStoreTextView);

        clothingAvailableTextView = (TextView) convertView.findViewById(R.id.clothingAvailablesTextView);
        QualificationRatingBar = (RatingBar) convertView.findViewById(R.id.QualificationRatingBar);
        addressStoreTextView = (TextView) convertView.findViewById(R.id.addressStoreTextView);

        clothingAvailableIconImageView = (ImageView) convertView.findViewById(R.id.clothingTypeIconImageView);
        qualificationIconImageView = (ImageView) convertView.findViewById(R.id.qualificationIconImageView);
        addressStoreIconImageView = (ImageView) convertView.findViewById(R.id.colorIconImageView);

        requestAsesorieButton = (Button) convertView.findViewById(R.id.requestAsesorieButton);
        requestAsesorieButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, "Proximamente contarás con chat personalizado", Toast.LENGTH_SHORT).show();
            }
        });

        requestReservationButton = (Button) convertView.findViewById(R.id.requestReservationButton);
        requestReservationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle arguments = new Bundle();
                android.app.DialogFragment dialogReservation = ReservationController.newInstance(arguments);
                dialogReservation.show(activity.getFragmentManager(), "Dialogo de reserva");
            }
        });

        if (mSelection.contains(position)) {
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.mi_blanco));
        } else {
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.mi_blanco));
        }

        Shops detailItem = detailFilterList.get(position);

        titleNameStoreTextView.setText(detailItem.Name);

        clothingAvailableTextView.setText(Integer.toString(detailItem.Count_Available));
        QualificationRatingBar.setMax(5);
        QualificationRatingBar.setRating(detailItem.Qualification);
        QualificationRatingBar.setNumStars(5);
        QualificationRatingBar.setEnabled(false);
        addressStoreTextView.setText(detailItem.Address);

        loadFontAndAnimations();

        return convertView;
    }

    private void loadFontAndAnimations(){
        try{
            Typeface helveticaNeueLight = Fonts.obtenerFontHelveticaNeueLight(activity);
            Typeface helveticaMedium = Fonts.obtenerFontHelveticaNeueMedium(activity);

            Animation transladar = AnimationUtils.loadAnimation(activity, R.anim.translate_from_right_to_left);
            transladar.setDuration(Fonts.timeAnimations());

            titleNameStoreTextView.setTypeface(helveticaMedium);

            titleClothingAvailableTextView.setTypeface(helveticaMedium);
            titleQualificationTextView.setTypeface(helveticaMedium);
            titleAddressStoreTextView.setTypeface(helveticaMedium);

            clothingAvailableTextView.setTypeface(helveticaNeueLight);
            addressStoreTextView.setTypeface(helveticaNeueLight);

            transladar.reset();
            //contenedorListaDia.startAnimation(transladar);

        }catch (Exception ex){
            Log.d("LoadFontAndAnimations", "ShopsFilterAdapter:" + ex.getLocalizedMessage());
        }
    }

    public void setNewSelection(int position) {
        mSelection.add(position);
        notifyDataSetChanged();
    }

    public Boolean contains(int position){
        if(mSelection.contains(position)){
            return true;
        }else{
            return false;
        }
    }

    public void removeSelection(int position) {
        mSelection.remove(Integer.valueOf(position));
        notifyDataSetChanged();
    }

    public void clearSelection() {
        mSelection = new ArrayList<Integer>();
        notifyDataSetChanged();
    }

    public int getSelectionCount() {
        return mSelection.size();
    }

    public ArrayList<Integer> getCurrentCheckedPosition() {
        return mSelection;
    }

    @Override
    public Filter getFilter() {
        if(searchSuppliesFilter == null) {
            searchSuppliesFilter = new SearchSuppliesFilter(detailFilterList);
        }
        return searchSuppliesFilter;
    }

    private class SearchSuppliesFilter extends Filter {

        private ArrayList<Shops> readList;

        public SearchSuppliesFilter(ArrayList<Shops> Readlist){
            this.readList = new ArrayList<Shops>();
            readList.addAll(Readlist);
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            if (constraint == null || constraint.length() == 0) {
                // No filter implemented we return all the list
                results.values = readList;
                results.count = readList.size();
            }
            else {
                // We perform filtering operation
                List<Shops> nReadlist = new ArrayList<Shops>();
                for (Shops p : readList) {
                    if(containsChars(p.Address, constraint.toString())){
                        nReadlist.add(p);
                    }
                    else if(containsChars(p.Name.toString(), constraint.toString())){
                        nReadlist.add(p);
                    }
                    else if(containsChars(Integer.toString(p.Qualification), constraint.toString())){
                        nReadlist.add(p);
                    }
                }
                synchronized(this) {
                    results.values = nReadlist;
                    results.count = nReadlist.size();
                }
            }
            return results;
        }

        private Boolean containsChars(String word, String wordToFound){
            boolean value = false;
            if(word.toUpperCase().startsWith(wordToFound.toUpperCase())){
                value = true;
                return  value;
            }
            if(word.toUpperCase().endsWith(wordToFound.toUpperCase())){
                value = true;
                return  value;
            }
            String[] words = word.split(" ");
            String[] wordsToFounds = wordToFound.split(" ");
            for (String text : words){
                for(String valueFound : wordsToFounds) {
                    if (text.toUpperCase().startsWith(valueFound.toUpperCase())) {
                        value = true;
                        return value;
                    }
                    if (text.toUpperCase().endsWith(valueFound.toUpperCase())) {
                        value = true;
                        return value;
                    }
                }
            }
            return value;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,FilterResults results) {
            // Now we have to inform the adapter about the new list filtered
            List<Shops> nReadList = (ArrayList<Shops>) results.values;
            notifyDataSetChanged();
            clear();
            for(int i = 0, l = nReadList.size(); i < l; i++)
                add(nReadList.get(i));
            notifyDataSetInvalidated();
        }
    }
}