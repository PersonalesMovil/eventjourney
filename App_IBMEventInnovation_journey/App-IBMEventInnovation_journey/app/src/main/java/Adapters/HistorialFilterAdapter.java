package Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.selectacg.daniel.ibmeventinnovation.R;

import java.util.ArrayList;
import java.util.List;

import Model.Records;
import Resource.Fonts;

/**
 * Created by Daniel on 16/06/16.
 */
public class HistorialFilterAdapter extends ArrayAdapter<Records> implements Filterable {

    private ArrayList<Integer> mSelection = new ArrayList<Integer>();
    private SearchSuppliesFilter searchSuppliesFilter;
    private ArrayList<Records> detailFilterList;
    private Activity activity;
    LinearLayout contenedorListaRecord;

    TextView titleNameStoreTextView;
    TextView titleClothingTypeTextView;
    TextView titlePurchaseDetailTextView;
    TextView titleDateTextView;
    TextView titleQualificationTextView;

    TextView nameStoreTextView;
    TextView descriptionEventTextView;
    TextView dateTextView;
    RatingBar QualificationRatingBar;
    TextView clothingTypeTextView;

    ImageView durationIconImageView;
    ImageView hourIconImageView;
    ImageView speakerIconImageView;


    public HistorialFilterAdapter(Activity context, ArrayList<Records> items) {
        super(context, R.layout.mi_historial_detalle, items);
        this.activity = context;
        this.detailFilterList = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            // This a new view we inflate the new layout
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.mi_historial_detalle, parent, false);
        }

        contenedorListaRecord = (LinearLayout) convertView.findViewById(R.id.contenedorListaHistorial);

        titleNameStoreTextView = (TextView) convertView.findViewById(R.id.titleNameStoreTextView);
        nameStoreTextView = (TextView) convertView.findViewById(R.id.nameStoreTextView);

        titlePurchaseDetailTextView = (TextView) convertView.findViewById(R.id.titleNameStoreTextView);
        descriptionEventTextView = (TextView) convertView.findViewById(R.id.descriptionTextView);

        titleDateTextView = (TextView) convertView.findViewById(R.id.titleAddressStoreTextView);
        dateTextView = (TextView) convertView.findViewById(R.id.dateTextView);

        titleClothingTypeTextView = (TextView) convertView.findViewById(R.id.titleClothingTypeTextView);
        clothingTypeTextView = (TextView) convertView.findViewById(R.id.clothingTypeTextView);

        titleQualificationTextView = (TextView) convertView.findViewById(R.id.titleDateTextView);
        QualificationRatingBar = (RatingBar) convertView.findViewById(R.id.QualificationRatingBar);

        hourIconImageView = (ImageView)  convertView.findViewById(R.id.storeIconImageView);
        speakerIconImageView = (ImageView) convertView.findViewById(R.id.clothingTypeIconImageView);
        durationIconImageView = (ImageView) convertView.findViewById(R.id.dateIconImageView);

        if (mSelection.contains(position)) {
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.mi_blanco));
        } else {
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.mi_blanco));
        }

        Records detailItem = detailFilterList.get(position);

        nameStoreTextView.setText(detailItem.Name_Store);
        dateTextView.setText(detailItem.Purchase_Date);
        clothingTypeTextView.setText(detailItem.Garment_Type);
        QualificationRatingBar.setNumStars(detailItem.Qualification);
        QualificationRatingBar.setMax(5);
        QualificationRatingBar.setRating(detailItem.Qualification);
        QualificationRatingBar.setNumStars(5);
        QualificationRatingBar.setEnabled(false);
        loadFontAndAnimations();

        return convertView;
    }

    private void loadFontAndAnimations(){
        try{
            Typeface helveticaNeueLight = Fonts.obtenerFontHelveticaNeueLight(activity);
            Typeface helveticaMedium = Fonts.obtenerFontHelveticaNeueMedium(activity);

            Animation transladar = AnimationUtils.loadAnimation(activity, R.anim.translate_from_right_to_left);
            transladar.setDuration(Fonts.timeAnimations());

            titlePurchaseDetailTextView.setTypeface(helveticaMedium);

            descriptionEventTextView.setTypeface(helveticaNeueLight);

            titleDateTextView.setTypeface(helveticaMedium);

            dateTextView.setTypeface(helveticaNeueLight);

            titleClothingTypeTextView.setTypeface(helveticaMedium);

            clothingTypeTextView.setTypeface(helveticaNeueLight);

            titleQualificationTextView.setTypeface(helveticaMedium);

            transladar.reset();
            //contenedorListaRecord.startAnimation(transladar);

        }catch (Exception ex){
            Log.d("LoadFontAndAnimations", "HistorialFilterAdapter:" + ex.getLocalizedMessage());
        }
    }

    public void setNewSelection(int position) {
        mSelection.add(position);
        notifyDataSetChanged();
    }

    public Boolean contains(int position){
        if(mSelection.contains(position)){
            return true;
        }else{
            return false;
        }
    }

    public void removeSelection(int position) {
        mSelection.remove(Integer.valueOf(position));
        notifyDataSetChanged();
    }

    public void clearSelection() {
        mSelection = new ArrayList<Integer>();
        notifyDataSetChanged();
    }

    public int getSelectionCount() {
        return mSelection.size();
    }

    public ArrayList<Integer> getCurrentCheckedPosition() {
        return mSelection;
    }

    @Override
    public Filter getFilter() {
        if(searchSuppliesFilter == null) {
            searchSuppliesFilter = new SearchSuppliesFilter(detailFilterList);
        }
        return searchSuppliesFilter;
    }

    private class SearchSuppliesFilter extends Filter {

        private ArrayList<Records> readList;

        public SearchSuppliesFilter(ArrayList<Records> Readlist){
            this.readList = new ArrayList<Records>();
            readList.addAll(Readlist);
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            if (constraint == null || constraint.length() == 0) {
                // No filter implemented we return all the list
                results.values = readList;
                results.count = readList.size();
            }
            else {
                // We perform filtering operation
                List<Records> nReadlist = new ArrayList<Records>();
                for (Records p : readList) {
                    if(containsChars(p.Garment_Type, constraint.toString())){
                        nReadlist.add(p);
                    }
                    else if(containsChars(p.Name_Store, constraint.toString())){
                        nReadlist.add(p);
                    }
                    else if(containsChars(Integer.toString(p.Qualification), constraint.toString())){
                        nReadlist.add(p);
                    }
                    else if(containsChars(p.Purchase_Date, constraint.toString())){
                        nReadlist.add(p);
                    }
                    else if(containsChars(p.Url_Photo, constraint.toString())){
                        nReadlist.add(p);
                    }
                }
                synchronized(this) {
                    results.values = nReadlist;
                    results.count = nReadlist.size();
                }
            }
            return results;
        }

        private Boolean containsChars(String word, String wordToFound){
            boolean value = false;
            if(word.toUpperCase().startsWith(wordToFound.toUpperCase())){
                value = true;
                return  value;
            }
            if(word.toUpperCase().endsWith(wordToFound.toUpperCase())){
                value = true;
                return  value;
            }
            String[] words = word.split(" ");
            String[] wordsToFounds = wordToFound.split(" ");
            for (String text : words){
                for(String valueFound : wordsToFounds) {
                    if (text.toUpperCase().startsWith(valueFound.toUpperCase())) {
                        value = true;
                        return value;
                    }
                    if (text.toUpperCase().endsWith(valueFound.toUpperCase())) {
                        value = true;
                        return value;
                    }
                }
            }
            return value;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,FilterResults results) {
            // Now we have to inform the adapter about the new list filtered
            List<Records> nReadList = (ArrayList<Records>) results.values;
            notifyDataSetChanged();
            clear();
            for(int i = 0, l = nReadList.size(); i < l; i++)
                add(nReadList.get(i));
            notifyDataSetInvalidated();
        }
    }
}




