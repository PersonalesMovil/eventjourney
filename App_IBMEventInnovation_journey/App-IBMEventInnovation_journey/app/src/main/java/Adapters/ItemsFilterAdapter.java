package Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.selectacg.daniel.ibmeventinnovation.R;

import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

import AsyncTask.DownloadShopsAsyncTask;
import Model.Items;
import Resource.Fonts;

/**
 * Created by Daniel on 23/07/16.
 */
public class ItemsFilterAdapter extends ArrayAdapter<Items> implements Filterable {

    private ArrayList<Integer> mSelection = new ArrayList<Integer>();
    private SearchSuppliesFilter searchSuppliesFilter;
    private ArrayList<Items> detailFilterList;
    private Activity activity;

    TextView titleNameStoreTextView;

    TextView titleClothingTypeTextView;
    TextView titleQualificationTextView;
    TextView titleColorTextView;
    TextView titleSizeTextView;
    TextView titlePriceTextView;

    TextView clothingTypeTextView;
    RatingBar QualificationRatingBar;
    TextView colorTextView;
    TextView sizeTextView;
    TextView priceTextView;

    ImageView nameStoreImageView;
    ImageView clothingTypeIconImageView;
    ImageView qualificationIconImageView;
    ImageView colorIconImageView;
    ImageView sizeIconImageView;
    ImageView priceIconImageView;

    Button storeAvailableButton;
    Button detailItemButton;

    RelativeLayout progressBar;

    LinearLayout contenedorListaDia;

    public ItemsFilterAdapter(Activity context, ArrayList<Items> items, RelativeLayout progressBar) {
        super(context, R.layout.prendas_similares_detalle, items);
        this.activity = context;
        this.detailFilterList = items;
        this.progressBar = progressBar;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            // This a new view we inflate the new layout
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.prendas_similares_detalle, parent, false);
        }

        contenedorListaDia = (LinearLayout) convertView.findViewById(R.id.contenedorListaDia);

        titleNameStoreTextView = (TextView) convertView.findViewById(R.id.titleNameStoreTextView);

        titleClothingTypeTextView = (TextView) convertView.findViewById(R.id.titleClothingTypeTextView);
        titleQualificationTextView = (TextView) convertView.findViewById(R.id.titleQualificationTextView);
        titleColorTextView = (TextView) convertView.findViewById(R.id.titleColorTextView);
        titleSizeTextView = (TextView) convertView.findViewById(R.id.titleSizeTextView);
        titlePriceTextView = (TextView) convertView.findViewById(R.id.titlePriceTextView);

        clothingTypeTextView = (TextView) convertView.findViewById(R.id.clothingTypeTextView);
        QualificationRatingBar = (RatingBar) convertView.findViewById(R.id.QualificationRatingBar);
        colorTextView = (TextView) convertView.findViewById(R.id.colorTextView);
        sizeTextView = (TextView) convertView.findViewById(R.id.sizeTextView);
        priceTextView = (TextView) convertView.findViewById(R.id.priceTextView);

        nameStoreImageView = (ImageView) convertView.findViewById(R.id.nameStoreImageView);
        clothingTypeIconImageView = (ImageView) convertView.findViewById(R.id.clothingTypeIconImageView);
        qualificationIconImageView = (ImageView) convertView.findViewById(R.id.qualificationIconImageView);
        colorIconImageView = (ImageView) convertView.findViewById(R.id.colorIconImageView);
        sizeIconImageView = (ImageView) convertView.findViewById(R.id.sizeIconImageView);
        priceIconImageView = (ImageView) convertView.findViewById(R.id.priceIconImageView);

        storeAvailableButton = (Button) convertView.findViewById(R.id.storeAvailableButton);
        detailItemButton = (Button) convertView.findViewById(R.id.detailItemButton);

        storeAvailableButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DownloadShopsAsyncTask downloadShopsAsyncTask = new DownloadShopsAsyncTask(activity,progressBar);
                downloadShopsAsyncTask.execute("DownloadShops");
            }
        });

        detailItemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity,"Se encuentra en desarrollo", Toast.LENGTH_SHORT).show();
            }
        });

        if (mSelection.contains(position)) {
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.mi_blanco));
        } else {
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.mi_blanco));
        }

        Items detailItem = detailFilterList.get(position);

        titleNameStoreTextView.setText("Prenda ");

        clothingTypeTextView.setText(detailItem.Gargament_Type);
        QualificationRatingBar.setMax(5);
        QualificationRatingBar.setRating(detailItem.Qualification);
        QualificationRatingBar.setNumStars(5);
        QualificationRatingBar.setEnabled(false);
        colorTextView.setText(detailItem.Color);
        sizeTextView.setText(detailItem.Size);
        priceTextView.setText(detailItem.Price);

        loadFontAndAnimations();

        return convertView;
    }

    private void loadFontAndAnimations(){
        try{
            Typeface helveticaNeueLight = Fonts.obtenerFontHelveticaNeueLight(activity);
            Typeface helveticaMedium = Fonts.obtenerFontHelveticaNeueMedium(activity);

            Animation transladar = AnimationUtils.loadAnimation(activity, R.anim.translate_from_right_to_left);
            transladar.setDuration(Fonts.timeAnimations());

            titleNameStoreTextView.setTypeface(helveticaMedium);
            titleClothingTypeTextView.setTypeface(helveticaMedium);
            titleQualificationTextView.setTypeface(helveticaMedium);
            titleColorTextView.setTypeface(helveticaMedium);
            titleSizeTextView.setTypeface(helveticaMedium);
            titlePriceTextView.setTypeface(helveticaMedium);

            clothingTypeTextView.setTypeface(helveticaNeueLight);
            colorTextView.setTypeface(helveticaNeueLight);
            sizeTextView.setTypeface(helveticaNeueLight);
            priceTextView.setTypeface(helveticaNeueLight);

            transladar.reset();
            //contenedorListaDia.startAnimation(transladar);

        }catch (Exception ex){
            Log.d("LoadFontAndAnimations", "EventDayFilterAdapter:" + ex.getLocalizedMessage());
        }
    }

    public void setNewSelection(int position) {
        mSelection.add(position);
        notifyDataSetChanged();
    }

    public Boolean contains(int position){
        if(mSelection.contains(position)){
            return true;
        }else{
            return false;
        }
    }

    public void removeSelection(int position) {
        mSelection.remove(Integer.valueOf(position));
        notifyDataSetChanged();
    }

    public void clearSelection() {
        mSelection = new ArrayList<Integer>();
        notifyDataSetChanged();
    }

    public int getSelectionCount() {
        return mSelection.size();
    }

    public ArrayList<Integer> getCurrentCheckedPosition() {
        return mSelection;
    }

    @Override
    public Filter getFilter() {
        if(searchSuppliesFilter == null) {
            searchSuppliesFilter = new SearchSuppliesFilter(detailFilterList);
        }
        return searchSuppliesFilter;
    }

    private class SearchSuppliesFilter extends Filter {

        private ArrayList<Items> readList;

        public SearchSuppliesFilter(ArrayList<Items> Readlist){
            this.readList = new ArrayList<Items>();
            readList.addAll(Readlist);
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            if (constraint == null || constraint.length() == 0) {
                // No filter implemented we return all the list
                results.values = readList;
                results.count = readList.size();
            }
            else {
                // We perform filtering operation
                List<Items> nReadlist = new ArrayList<Items>();
                for (Items p : readList) {
                    if(containsChars(p.Brand, constraint.toString())){
                        nReadlist.add(p);
                    }
                    else if(containsChars(p.Color.toString(), constraint.toString())){
                        nReadlist.add(p);
                    }
                    else if(containsChars(p.Gargament_Type.toString(), constraint.toString())){
                        nReadlist.add(p);
                    }
                    else if(containsChars(Integer.toString(p.Qualification), constraint.toString())){
                        nReadlist.add(p);
                    }
                    else if(containsChars(p.Size.toString(), constraint.toString())){
                        nReadlist.add(p);
                    }
                }
                synchronized(this) {
                    results.values = nReadlist;
                    results.count = nReadlist.size();
                }
            }
            return results;
        }

        private Boolean containsChars(String word, String wordToFound){
            boolean value = false;
            if(word.toUpperCase().startsWith(wordToFound.toUpperCase())){
                value = true;
                return  value;
            }
            if(word.toUpperCase().endsWith(wordToFound.toUpperCase())){
                value = true;
                return  value;
            }
            String[] words = word.split(" ");
            String[] wordsToFounds = wordToFound.split(" ");
            for (String text : words){
                for(String valueFound : wordsToFounds) {
                    if (text.toUpperCase().startsWith(valueFound.toUpperCase())) {
                        value = true;
                        return value;
                    }
                    if (text.toUpperCase().endsWith(valueFound.toUpperCase())) {
                        value = true;
                        return value;
                    }
                }
            }
            return value;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,FilterResults results) {
            // Now we have to inform the adapter about the new list filtered
            List<Items> nReadList = (ArrayList<Items>) results.values;
            notifyDataSetChanged();
            clear();
            for(int i = 0, l = nReadList.size(); i < l; i++)
                add(nReadList.get(i));
            notifyDataSetInvalidated();
        }
    }
}