package Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.selectacg.daniel.ibmeventinnovation.R;

import java.util.ArrayList;
import java.util.List;

import Model.Orders;
import Resource.Fonts;

/**
 * Created by SelectaCG on 28/07/15.
 */

/*
    Adaptador que se encarga de cargar los items de los dias
 */
public class PedidosFilterAdapter extends ArrayAdapter<Orders> implements Filterable{

    private ArrayList<Integer> mSelection = new ArrayList<Integer>();
    private SearchSuppliesFilter searchSuppliesFilter;
    private ArrayList<Orders> detailFilterList;
    private Activity currentContext;

    TextView titleReservationDetailTextView;

    TextView titleNameStoreTextView;
    TextView titleClothingTypeTextView;
    TextView titleQualificationTextView;
    TextView titleDateTextView;
    TextView titleAddressStoreTextView;
    TextView titleStatusOrderTextView;

    TextView nameStoreTextView;
    TextView clothingTypeTextView;
    RatingBar QualificationRatingBar;
    TextView dateTextView;
    TextView addressStoreTextView;
    TextView statusOrderTextView;

    ImageView nameStoreImageView;
    ImageView clothingTypeIconImageView;
    ImageView qualificationIconImageView;
    ImageView dateIconImageView;
    ImageView addressIcoImageView;
    ImageView statusPedidoImageView;

    Button confirmReservationButton;
    Button cancelReservationButton;

    LinearLayout contenedorListaDia;

    LinearLayout statusOrderLinearLayout;
    LinearLayout buttonsStatusLinearLayout;

    RelativeLayout progressBar;
    int currentPos;

    public PedidosFilterAdapter(Activity context, ArrayList<Orders> items, RelativeLayout progressBar) {
        super(context, R.layout.mis_pedidos_detalle, items);
        this.currentContext = context;
        this.detailFilterList = items;
        this.progressBar = progressBar;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            // This a new view we inflate the new layout
            LayoutInflater inflater = (LayoutInflater) currentContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.mis_pedidos_detalle, parent, false);
        }

        contenedorListaDia = (LinearLayout) convertView.findViewById(R.id.contenedorListaDia);
        titleReservationDetailTextView = (TextView) convertView.findViewById(R.id.titleReservationDetailTextView);
        titleNameStoreTextView = (TextView) convertView.findViewById(R.id.titleNameStoreTextView);
        titleClothingTypeTextView = (TextView) convertView.findViewById(R.id.titleClothingTypeTextView);
        titleQualificationTextView = (TextView) convertView.findViewById(R.id.titleQualificationTextView);
        titleDateTextView = (TextView) convertView.findViewById(R.id.titleDateTextView);
        titleAddressStoreTextView = (TextView) convertView.findViewById(R.id.titleAddressStoreTextView);
        titleStatusOrderTextView =  (TextView) convertView.findViewById(R.id.titleStatusOrderTextView);

        nameStoreTextView = (TextView) convertView.findViewById(R.id.nameStoreTextView);
        clothingTypeTextView = (TextView) convertView.findViewById(R.id.clothingTypeTextView);
        QualificationRatingBar = (RatingBar) convertView.findViewById(R.id.QualificationRatingBar);
        dateTextView = (TextView) convertView.findViewById(R.id.dateTextView);
        addressStoreTextView = (TextView) convertView.findViewById(R.id.addressStoreTextView);
        statusOrderTextView =  (TextView) convertView.findViewById(R.id.statusOrderTextView);

        nameStoreImageView = (ImageView) convertView.findViewById(R.id.nameStoreImageView);
        clothingTypeIconImageView = (ImageView) convertView.findViewById(R.id.clothingTypeIconImageView);
        qualificationIconImageView = (ImageView) convertView.findViewById(R.id.qualificationIconImageView);
        dateIconImageView = (ImageView) convertView.findViewById(R.id.dateIconImageView);
        addressIcoImageView = (ImageView) convertView.findViewById(R.id.addressIcoImageView);
        statusPedidoImageView = (ImageView) convertView.findViewById(R.id.statusPedidoImageView);

        statusOrderLinearLayout = (LinearLayout) convertView.findViewById(R.id.statusOrderLinearLayout);
        buttonsStatusLinearLayout = (LinearLayout) convertView.findViewById(R.id.buttonsStatusLinearLayout);

        confirmReservationButton = (Button) convertView.findViewById(R.id.confirmReservationButton);
        confirmReservationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Orders detailItem = detailFilterList.get(currentPos);
                    detailItem.isConfirm = true;
                    buttonsStatusLinearLayout.setVisibility(View.INVISIBLE);
                    statusOrderTextView.setText("Confirmada");
                }catch (Exception ex){
                    Log.d("ConfirmReservation","Error: "  + ex.getMessage());
                }
            }
        });

        cancelReservationButton = (Button) convertView.findViewById(R.id.cancelReservationButton);
        cancelReservationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    statusOrderTextView.setText("Cancelada");
                    buttonsStatusLinearLayout.setVisibility(View.INVISIBLE);
                }catch (Exception ex){
                    Log.d("CancelReservation","Error: " + ex.getMessage());
                }
            }
        });

        if (mSelection.contains(position)) {
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.mi_blanco));
        } else {
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.mi_blanco));
        }

        Orders detailItem = detailFilterList.get(position);
        currentPos = position;
        nameStoreTextView.setText(detailItem.Name_Store);
        clothingTypeTextView.setText(detailItem.Garment_Type);
        QualificationRatingBar.setMax(5);
        QualificationRatingBar.setRating(detailItem.Qualification);
        QualificationRatingBar.setNumStars(5);
        QualificationRatingBar.setEnabled(false);
        dateTextView.setText(detailItem.Purchase_Date);
        addressStoreTextView.setText(detailItem.Address_Store);
        if(detailItem.isConfirm){
            buttonsStatusLinearLayout.setVisibility(View.INVISIBLE);
            statusOrderTextView.setText("Confirmada");
        }else if(detailItem.isCancel){
            statusOrderTextView.setText("Cancelada");
            buttonsStatusLinearLayout.setVisibility(View.INVISIBLE);
        }else{
            statusOrderTextView.setText("Sin Confirmar");
            buttonsStatusLinearLayout.setVisibility(View.VISIBLE);
        }
        loadFontAndAnimations();

        return convertView;
    }

    private void loadFontAndAnimations(){
        try{
            Typeface helveticaNeueLight = Fonts.obtenerFontHelveticaNeueLight(currentContext);
            Typeface helveticaMedium = Fonts.obtenerFontHelveticaNeueMedium(currentContext);

            Animation transladar = AnimationUtils.loadAnimation(currentContext, R.anim.translate_from_right_to_left);
            transladar.setDuration(Fonts.timeAnimations());

            titleReservationDetailTextView.setTypeface(helveticaMedium);
            titleNameStoreTextView.setTypeface(helveticaMedium);
            titleClothingTypeTextView.setTypeface(helveticaMedium);
            titleQualificationTextView.setTypeface(helveticaMedium);
            titleDateTextView.setTypeface(helveticaMedium);
            titleAddressStoreTextView.setTypeface(helveticaMedium);

            nameStoreTextView.setTypeface(helveticaNeueLight);
            clothingTypeTextView.setTypeface(helveticaNeueLight);
            dateTextView.setTypeface(helveticaNeueLight);
            addressStoreTextView.setTypeface(helveticaNeueLight);

            transladar.reset();
            //contenedorListaDia.startAnimation(transladar);

        }catch (Exception ex){
            Log.d("LoadFontAndAnimations", "EventDayFilterAdapter:" + ex.getLocalizedMessage());
        }
    }

    public void setNewSelection(int position) {
        mSelection.add(position);
        notifyDataSetChanged();
    }

    public Boolean contains(int position){
        if(mSelection.contains(position)){
            return true;
        }else{
            return false;
        }
    }

    public void removeSelection(int position) {
        mSelection.remove(Integer.valueOf(position));
        notifyDataSetChanged();
    }

    public void clearSelection() {
        mSelection = new ArrayList<Integer>();
        notifyDataSetChanged();
    }

    public int getSelectionCount() {
        return mSelection.size();
    }

    public ArrayList<Integer> getCurrentCheckedPosition() {
        return mSelection;
    }

    @Override
    public Filter getFilter() {
        if(searchSuppliesFilter == null) {
            searchSuppliesFilter = new SearchSuppliesFilter(detailFilterList);
        }
        return searchSuppliesFilter;
    }

    private class SearchSuppliesFilter extends Filter {

        private ArrayList<Orders> readList;

        public SearchSuppliesFilter(ArrayList<Orders> Readlist){
            this.readList = new ArrayList<Orders>();
            readList.addAll(Readlist);
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            if (constraint == null || constraint.length() == 0) {
                // No filter implemented we return all the list
                results.values = readList;
                results.count = readList.size();
            }
            else {
                // We perform filtering operation
                List<Orders> nReadlist = new ArrayList<Orders>();
                for (Orders p : readList) {
                    if(containsChars(p.Address_Store, constraint.toString())){
                        nReadlist.add(p);
                    }
                    else if(containsChars(p.Garment_Type.toString(), constraint.toString())){
                        nReadlist.add(p);
                    }
                    else if(containsChars(p.Name_Store.toString(), constraint.toString())){
                        nReadlist.add(p);
                    }
                    else if(containsChars(Integer.toString(p.Qualification), constraint.toString())){
                        nReadlist.add(p);
                    }
                    else if(containsChars(p.Url_Photo.toString(), constraint.toString())){
                        nReadlist.add(p);
                    }
                }
                synchronized(this) {
                    results.values = nReadlist;
                    results.count = nReadlist.size();
                }
            }
            return results;
        }

        private Boolean containsChars(String word, String wordToFound){
            boolean value = false;
            if(word.toUpperCase().startsWith(wordToFound.toUpperCase())){
                value = true;
                return  value;
            }
            if(word.toUpperCase().endsWith(wordToFound.toUpperCase())){
                value = true;
                return  value;
            }
            String[] words = word.split(" ");
            String[] wordsToFounds = wordToFound.split(" ");
            for (String text : words){
                for(String valueFound : wordsToFounds) {
                    if (text.toUpperCase().startsWith(valueFound.toUpperCase())) {
                        value = true;
                        return value;
                    }
                    if (text.toUpperCase().endsWith(valueFound.toUpperCase())) {
                        value = true;
                        return value;
                    }
                }
            }
            return value;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,FilterResults results) {
            // Now we have to inform the adapter about the new list filtered
            List<Orders> nReadList = (ArrayList<Orders>) results.values;
            notifyDataSetChanged();
            clear();
            for(int i = 0, l = nReadList.size(); i < l; i++)
                add(nReadList.get(i));
            notifyDataSetInvalidated();
        }
    }
}



