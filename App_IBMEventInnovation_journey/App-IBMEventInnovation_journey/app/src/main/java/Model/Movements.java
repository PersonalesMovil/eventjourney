package Model;

/**
 * Created by Daniel on 20/06/16.
 */
public class Movements {

    public String key;
    public String Id;
    public String Email;
    public String Hour_Available;
    public Integer Level;
    public String Name_Router;
    public String Hour_Disable;

    public Movements(){

    }

    public Movements(String Email, String Hour_Available, Integer Level, String Name_Router, String Hour_Disable){
        this.Email = Email;
        this.Hour_Available = Hour_Available;
        this.Level = Level;
        this.Name_Router = Name_Router;
        this.Hour_Disable = Hour_Disable;
    }
}
