package Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by SelectaCG on 14/09/15.
 */
public class Photos implements Parcelable {

     //Nombre del fichero de la foto
     private String nombreFichero;
    //String con contenido de la foto. Es opcional lo hemos dejado para pruebas
    private String contenidoFichero;
    //String en formato Base64, es el que en teoría deben usar para pasarnos el binario del jpg de la foto
    private byte[] contenidoBase64;
    //Ruta guardado
    public String currentPATH;

    public Photos(){
        super();
    }

    public Photos(Parcel in) {
        readFromParcel(in);
    }

    public Photos(String nombreFichero, byte[] contenidoBase64){
        super();
        this.nombreFichero = nombreFichero;
        this.contenidoBase64 = contenidoBase64;
    }

    public String getNombreFichero() {
        return nombreFichero;
    }

    public void setNombreFichero(String nombreFichero) {
        this.nombreFichero = nombreFichero;
    }

    public String getContenidoFichero() {
        return contenidoFichero;
    }

    public void setContenidoFichero(String contenidoFichero) {
        this.contenidoFichero = contenidoFichero;
    }

    public byte[] getContenidoBase64() {
        return contenidoBase64;
    }

    public void setContenidoBase64(byte[] contenidoBase64) {
        this.contenidoBase64 = contenidoBase64;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nombreFichero);
        dest.writeByteArray(this.contenidoBase64);
    }

    public void readFromParcel(Parcel in)
    {
        this.nombreFichero = in.readString();
        this.contenidoBase64 = in.createByteArray();
    }

    public static final Parcelable.Creator<Photos> CREATOR = new Parcelable.Creator<Photos>()
    {
        @Override
        public Photos createFromParcel(Parcel in)
        {
            return new Photos(in);
        }

        @Override
        public Photos[] newArray(int size)
        {
            return new Photos[size];
        }
    };
}
