package Model;

/**
 * Created by Daniel on 16/06/16.
 */

public class Events {

    public String Date;
    public String Description;
    public String Hour;
    public String Speaker;
    public String Title;
    public Integer day;
    public Integer duration;
    public Integer id_event;
    public String Image_Url;

    public Events(){

    }

    public Events(String Date, String Description, String Hour, String Speaker, String Title, Integer Day, Integer Duration, Integer Id_Event){
        this.Date = Date;
        this.Description = Description;
        this.Hour = Hour;
        this.Speaker = Speaker;
        this.Title = Title;
        this.day = Day;
        this.duration = Duration;
        this.id_event = Id_Event;
    }
}
