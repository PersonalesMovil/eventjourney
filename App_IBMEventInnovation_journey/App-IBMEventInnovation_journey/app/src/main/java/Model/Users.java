package Model;

import com.twitter.sdk.android.core.models.User;

/**
 * Created by Daniel on 16/06/16.
 */
public class Users {

    public String Id;
    public String Company;
    public String Email;
    public String Gender;
    public String Last_Name;
    public String Name;
    public String Hour_Connection;

    private static Users instance;

    private Users(){

    }

    private Users(String Id, String Name, String Last_name, String Email, String Gender, String Company){
        this.Id = Id;
        this.Name = Name;
        this.Email = Email;
        this.Last_Name = Last_name;
        this.Gender = Gender;
        this.Company = Company;
    }

    public static Users newInstance() {
        if(instance == null){
            instance = new Users();
        }
        return instance;
    }

    public static Users newInstance(String Id,String Name, String Last_name, String Email, String Gender, String Company) {
        if(instance == null){
            instance = new Users(Id,Name,Last_name, Email,Gender,Company);
        }
        return instance;
    }
}
