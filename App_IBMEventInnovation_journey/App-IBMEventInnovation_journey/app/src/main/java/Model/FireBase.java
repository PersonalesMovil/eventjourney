package Model;

import android.util.Log;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;

import java.util.ArrayList;

import Enum.EntitiesName;

/**
 * Created by Daniel on 16/06/16.
 */
public class FireBase {

    String urlBase;
    Firebase myFireBase;

    public FireBase(){
        urlBase = "https://ibmeventinnovation.firebaseio.com";
    }

    public Boolean registerUser(Users user){
        try {
            myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Users);
            Firebase userRef = myFireBase.child(EntitiesName.Users);
            userRef.setValue(user);
        }catch (Exception ex){
            Log.e("RegisterUser",ex.getMessage());
        }
        return true;
    }

    public Firebase getUrlUsers(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Users);
        return myFireBase;
    }

    public Firebase getUrlEvents(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Events);
        return myFireBase;
    }

    public Firebase getUrlMovements(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Movements);
        return myFireBase;
    }

    public Firebase getUrlRouters(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Routers);
        return myFireBase;
    }

    public Firebase getUrlNotificationsEvents(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.NotificationsEvents);
        return myFireBase;
    }

    public Firebase getUrlSettings(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Settings);
        return myFireBase;
    }

    public Firebase getUrlRecords(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Records);
        return myFireBase;
    }

    public Firebase getUrlOrders(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Orders);
        return myFireBase;
    }

    public Firebase getUrlShops(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Shops);
        return myFireBase;
    }

    public Firebase getUrlItems(){
        myFireBase = new Firebase(urlBase + "/" + EntitiesName.Root + "/" + EntitiesName.Items);
        return myFireBase;
    }
}
