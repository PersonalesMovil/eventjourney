package Model;

/**
 * Created by Daniel on 16/06/16.
 */
public class Routers {
    public String Name;
    public Integer Level;
    public String IsMessage;
    public String Message;
    public String Date;
    public Integer Duration;
    public String Hour;
    public String Image_Url;
    public String Speaker;
    public String Title;
    public Boolean notified;

    public Routers(){

    }

    public Routers(String Name, Integer levelInit, String IsMessage, String Message){
        this.Name = Name;
        this.Level = levelInit;
        this.IsMessage = IsMessage;
        this.Message = Message;
    }

}
