package AsyncTask;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.selectacg.daniel.ibmeventinnovation.Controllers.PrendasSimilaresController;
import com.selectacg.daniel.ibmeventinnovation.Controllers.TiendasControllers;
import com.selectacg.daniel.ibmeventinnovation.R;

import java.util.ArrayList;

import Model.FireBase;
import Model.Shops;
import Resource.Connectivity;
import Resource.DataUser;
import Resource.MetodosUtiles;

/**
 * Created by Daniel on 23/07/16.
 */
public class DownloadShopsAsyncTask  extends AsyncTask<String, String, String> {

    //Se solicita la actividad actual de la aplicacion para acceder el main activity
    Activity activity;
    RelativeLayout progressHUD;
    TextView mensajeTextView;

    @Override
    protected String doInBackground(String... params) {
        try {
            publishProgress(activity.getString(R.string.msj_verified_network));
            if (Connectivity.isConnectedNetWork(activity)) {
                publishProgress(activity.getString(R.string.msj_download_shops_availables));
                getAllShops();
            } else {
                publishProgress(activity.getString(R.string.msj_is_it_not_connected_network));
            }
        } catch (Exception ex) {
            publishProgress("Error:" + ex.getMessage());
            MetodosUtiles.mostrarPausaMensaje();
        }
        return "Validacion exitosa";
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressHUD.setVisibility(View.VISIBLE);
        mensajeTextView = (TextView) progressHUD.findViewById(R.id.mensajeTextView);
        mensajeTextView.setText(activity.getString(R.string.msj_wait));
    }

    @Override
    protected void onPostExecute(String s) {
        try {
            super.onPostExecute(s);
        } catch (Exception ex) {
            Log.d("DownloadItems","Error: " + ex.getMessage());
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        mensajeTextView.setText(values[0]);
    }

    public DownloadShopsAsyncTask(Activity activity, RelativeLayout progressHUD) {
        super();
        this.activity = activity;
        this.progressHUD = progressHUD;
    }

    public void getAllShops(){
        FireBase myFireBase = new FireBase();
        myFireBase.getUrlShops().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                try {
                    ArrayList<Shops> listShops = new ArrayList<>();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        Shops Order = postSnapshot.getValue(Shops.class);
                        listShops.add(Order);
                    }
                    Log.d("Shops Download:"," "+ listShops.size());
                    if (listShops.size() > 0) {
                        DataUser dataUser = DataUser.newInstance();
                        dataUser.ShopsList = listShops;
                        //Se debe llamar la actividad que mostrara la lista de dias programados
                        Intent MandantSelection = new Intent(activity, TiendasControllers.class);
                        activity.startActivity(MandantSelection);
                        progressHUD.setVisibility(View.INVISIBLE);
                    }else{
                        Toast.makeText(activity,"No se encontraron Tiendas disponibles", Toast.LENGTH_SHORT).show();
                        progressHUD.setVisibility(View.INVISIBLE);
                    }
                }catch (Exception ex) {
                    Log.e("LoadSimilarItems", ex.getMessage());
                }
            }
            @Override public void onCancelled(FirebaseError error) {
                System.out.println("Details: " + error.getDetails());
            }
        });
    }

}
