package AsyncTask;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.selectacg.daniel.ibmeventinnovation.Controllers.PedidosController;
import com.selectacg.daniel.ibmeventinnovation.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import Model.Events;
import Model.FireBase;
import Model.Orders;
import Model.Users;
import Resource.Connectivity;
import Resource.DataUser;
import Resource.MetodosUtiles;
import Resource.PrefUtils;

/**
 * Created by Daniel on 23/07/16.
 */
public class DownloadOrdersAsyncTask extends AsyncTask<String, String, String> {

    //Se solicita la actividad actual de la aplicacion para acceder el main activity
    Activity activity;
    RelativeLayout progressHUD;
    TextView mensajeTextView;
    android.app.DialogFragment dialogReservation;

    @Override
    protected String doInBackground(String... params) {
        try {
            publishProgress(activity.getString(R.string.msj_verified_network));
            if (Connectivity.isConnectedNetWork(activity)) {
                publishProgress(activity.getString(R.string.msj_download_orders));
                getAllOrders();
            } else {
                publishProgress(activity.getString(R.string.msj_is_it_not_connected_network));
            }
        } catch (Exception ex) {
            publishProgress("Error:" + ex.getMessage());
            MetodosUtiles.mostrarPausaMensaje();
        }
        return "Validacion exitosa";
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressHUD.setVisibility(View.VISIBLE);
        mensajeTextView = (TextView) progressHUD.findViewById(R.id.mensajeTextView);
        mensajeTextView.setText(activity.getString(R.string.msj_wait));
    }

    @Override
    protected void onPostExecute(String s) {
        try {
            super.onPostExecute(s);
        } catch (Exception ex) {
            Log.d("DownloadOrders","Error: " + ex.getMessage());
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        mensajeTextView.setText(values[0]);
    }

    public DownloadOrdersAsyncTask(Activity activity, RelativeLayout progressHUD) {
        super();
        this.activity = activity;
        this.progressHUD = progressHUD;
    }

    public DownloadOrdersAsyncTask(Activity activity, RelativeLayout progressHUD, android.app.DialogFragment dialogReservation) {
        super();
        this.activity = activity;
        this.progressHUD = progressHUD;
        this.dialogReservation = dialogReservation;
    }

    public void getAllOrders(){
        FireBase myFireBase = new FireBase();
        final android.app.DialogFragment dialogReservation = this.dialogReservation;
        myFireBase.getUrlOrders().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                try {
                    ArrayList<Orders> listOrders = new ArrayList<>();
                    Users currenUser = PrefUtils.getCurrentUser(activity);;

                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        Orders Order = postSnapshot.getValue(Orders.class);
                        try {
                            //if(Order.email.trim().equalsIgnoreCase(currenUser.Email.trim())) {
                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                                String str1 = MetodosUtiles.date_format("yyyy-MM-dd HH:mm");
                                Date date1 = formatter.parse(str1);

                                String str2 = Order.Purchase_Date;
                                Date date2 = formatter.parse(str2);

                                if (date1.compareTo(date2) < 0 && !Order.isNotified) {
                                    listOrders.add(Order);
                                }
                            //}
                        } catch (ParseException e1) {
                           Log.d("Parse" , "Error:" + e1.getMessage());
                        }
                    }
                    Log.d("Orders Download:"," "+ listOrders.size());
                    if (listOrders.size() > 0) {
                        DataUser dataUser = DataUser.newInstance();
                        dataUser.ordersList = listOrders;
                        //Se debe llamar la actividad que mostrara la lista de dias programados
                        progressHUD.setVisibility(View.INVISIBLE);
                        Intent MandantSelection = new Intent(activity, PedidosController.class);
                        activity.startActivity(MandantSelection);
                    }else{
                        progressHUD.setVisibility(View.INVISIBLE);
                        Toast.makeText(activity,"No se encontraron pedidos activos",Toast.LENGTH_SHORT).show();
                    }
                    if(dialogReservation != null){
                        dialogReservation.dismiss();
                    }
                }catch (Exception ex) {
                    Log.e("LoadEvents", ex.getMessage());
                }
            }
            @Override public void onCancelled(FirebaseError error) {
                System.out.println("Details: " + error.getDetails());
            }
        });
    }
}