package AsyncTask;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.selectacg.daniel.ibmeventinnovation.Controllers.HomeController;
import com.selectacg.daniel.ibmeventinnovation.Controllers.RegistroController;
import com.selectacg.daniel.ibmeventinnovation.R;

import java.util.ArrayList;

import Model.FireBase;
import Model.Users;
import Resource.Connectivity;
import Resource.DataUser;
import Resource.MetodosUtiles;


/**
 * Created by Daniel on 27/11/15.
 * Clase asincrona que permite obtener la autenticacion del usuario, de ser valida procede a validar su perfil, de
 * ser invalida no continua con el proceso
 */
public class AutenticateAsyncTask extends AsyncTask<Users, String, String> {

    //Se solicita la actividad actual de la aplicacion para acceder el main activity
    Activity activity;
    RelativeLayout contentProgressBar;
    TextView mensajeTextView;
    Users user;

    @Override
    protected String doInBackground(Users... params) {
        user = params[0];
        try {
            publishProgress(activity.getString(R.string.msj_verified_network));
            if(Connectivity.isConnectedNetWork(activity)) {
                publishProgress(activity.getString(R.string.msj_authenticating_user));
                authenticate(user);
            }else{
                publishProgress(activity.getString(R.string.msj_is_it_not_connected_network));

            }
        }catch (Exception ex){
            publishProgress("Error:" + ex.getMessage());
            MetodosUtiles.mostrarPausaMensaje();
        }
        return "Validacion exitosa";
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        contentProgressBar.setVisibility(View.VISIBLE);
        mensajeTextView = (TextView) contentProgressBar.findViewById(R.id.mensajeTextView);
        mensajeTextView.setText(activity.getString(R.string.msj_wait));
    }

    @Override
    protected void onPostExecute(String s){
        try {
            if(user != null) {
                super.onPostExecute(s);
            }else{
                contentProgressBar.setVisibility(View.INVISIBLE);
            }
        }catch (Exception ex){
            Toast.makeText(activity, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onProgressUpdate(String... values){
        super.onProgressUpdate(values);
        mensajeTextView.setText(values[0]);
    }

    public AutenticateAsyncTask(Activity activity, RelativeLayout contentProgressBar) {
        super();
        this.activity = activity;
        this.contentProgressBar = contentProgressBar;
    }

    public void authenticate(final Users user){
        FireBase myFireBase = new FireBase();
        myFireBase.getUrlUsers().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    ArrayList<Users> listUsers = new ArrayList<>();
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        Users event = postSnapshot.getValue(Users.class);
                        if(event.Email.equalsIgnoreCase(user.Email)) {
                            listUsers.add(event);
                        }
                    }
                    if (listUsers.size() > 0) {
                        for (Users current : listUsers) {
                            if (current.Email.equalsIgnoreCase(user.Email)) {
                                user.Hour_Connection = MetodosUtiles.date_format("HH:mm");
                                DataUser dataUser = DataUser.newInstance();
                                dataUser.userConnect = user;
                                dataUser.loginManual = true;
                                dataUser.loginWithFacebook = false;
                                dataUser.loginwithTwitter = false;

                                Intent MandantSelection = new Intent(activity, HomeController.class);
                                activity.startActivity(MandantSelection);
                            }
                        }


                    } else {
                        Intent MandantSelection = new Intent(activity, RegistroController.class);
                        MandantSelection.putExtra("Email", AutenticateAsyncTask.this.user.Email);
                        MandantSelection.putExtra("Password", AutenticateAsyncTask.this.user.Id);
                        activity.startActivity(MandantSelection);

                    }
                    contentProgressBar.setVisibility(View.INVISIBLE);
                }catch (Exception ex){
                    contentProgressBar.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                System.out.println(firebaseError.getDetails());
            }
        });
    }
}
