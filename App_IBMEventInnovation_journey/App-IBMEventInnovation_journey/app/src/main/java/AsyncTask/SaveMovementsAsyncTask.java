package AsyncTask;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.selectacg.daniel.ibmeventinnovation.R;

import Model.FireBase;
import Model.Movements;
import Resource.Connectivity;
import Resource.MetodosUtiles;

/**
 * Created by Daniel on 20/06/16.
 */
public class SaveMovementsAsyncTask extends AsyncTask<Movements, String, String> {

    //Se solicita la actividad actual de la aplicacion para acceder el main activity
    Activity activity;
    RelativeLayout progressHUD;
    TextView mensajeTextView;
    Movements movements;

    @Override
    protected String doInBackground(Movements... params) {
        movements = params[0];
        try {
            publishProgress(activity.getString(R.string.msj_verified_network));
            if (Connectivity.isConnectedNetWork(activity)) {
                saveMovements(movements);
            } else {
                publishProgress(activity.getString(R.string.msj_is_it_not_connected_network));
            }
        } catch (Exception ex) {
            publishProgress("Error:" + ex.getMessage());
            MetodosUtiles.mostrarPausaMensaje();
        }
        return "Validacion exitosa";
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(progressHUD != null) {
             progressHUD.setVisibility(View.VISIBLE);
             //contentPage.setVisibility(View.INVISIBLE);
            mensajeTextView = (TextView) progressHUD.findViewById(R.id.mensajeTextView);
            mensajeTextView.setText(activity.getString(R.string.msj_wait));
        }
    }

    @Override
    protected void onPostExecute(String s) {
        try {
            if (movements != null) {
                super.onPostExecute(s);
            } else {
                if(progressHUD != null) {
                    progressHUD.setVisibility(View.INVISIBLE);
                }
            }
        } catch (Exception ex) {
            Toast.makeText(activity, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        if(mensajeTextView != null) {
            mensajeTextView.setText(values[0]);
        }
    }

    public SaveMovementsAsyncTask(Activity activity ) {
        super();
        this.activity = activity;
    }

    public Boolean saveMovements(Movements movements){
        try {
            FireBase myFireBase = new FireBase();
            myFireBase.getUrlMovements().child(movements.Hour_Available).setValue(movements, new Firebase.CompletionListener() {
                @Override
                public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                    if(firebaseError != null){
                        Log.e("errorSaveMovements", firebaseError.getMessage());
                    }else{
                        Log.d("Movements save suces:","Firebase");
                    }
                    if(progressHUD != null) {
                        progressHUD.setVisibility(View.INVISIBLE);
                    }
                }
            });
        }catch (Exception ex){
            Log.e("RegisterUser",ex.getMessage());
        }
        return true;
    }
}
