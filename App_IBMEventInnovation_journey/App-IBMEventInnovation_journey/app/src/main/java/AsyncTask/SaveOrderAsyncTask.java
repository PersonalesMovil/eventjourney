package AsyncTask;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.selectacg.daniel.ibmeventinnovation.R;

import Model.FireBase;
import Model.Orders;
import Resource.Connectivity;
import Resource.MetodosUtiles;

/**
 * Created by Daniel on 24/07/16.
 */
public class SaveOrderAsyncTask extends AsyncTask<Orders, String, String> {

    //Se solicita la actividad actual de la aplicacion para acceder el main activity
    Activity activity;
    RelativeLayout progressHUD;
    TextView mensajeTextView;
    Orders orders;
    android.app.DialogFragment dialogReservation;

    @Override
    protected String doInBackground(Orders... params) {
        orders = params[0];
        try {
            publishProgress(activity.getString(R.string.msj_verified_network));
            if (Connectivity.isConnectedNetWork(activity)) {
                publishProgress(activity.getString(R.string.msj_saving_orders));
                saveOrders(orders);
            } else {
                publishProgress(activity.getString(R.string.msj_is_it_not_connected_network));
            }
        } catch (Exception ex) {
            publishProgress("Error:" + ex.getMessage());
            MetodosUtiles.mostrarPausaMensaje();
        }
        return "Validacion exitosa";
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(progressHUD != null) {
            progressHUD.setVisibility(View.VISIBLE);
            mensajeTextView = (TextView) progressHUD.findViewById(R.id.mensajeTextView);
            mensajeTextView.setText(activity.getString(R.string.msj_wait));
        }
    }

    @Override
    protected void onPostExecute(String s) {
        try {
            super.onPostExecute(s);
        } catch (Exception ex) {
            Toast.makeText(activity, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        if(mensajeTextView != null) {
            mensajeTextView.setText(values[0]);
        }
    }

    public SaveOrderAsyncTask(Activity activity, android.app.DialogFragment dialogReservation, RelativeLayout progressHUD ) {
        super();
        this.activity = activity;
        this.dialogReservation = dialogReservation;
        this.progressHUD = progressHUD;
    }

    public Boolean saveOrders(Orders orders){
        try {
            FireBase myFireBase = new FireBase();
            myFireBase.getUrlOrders().push().setValue(orders, new Firebase.CompletionListener() {
                @Override
                public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                    if(firebaseError != null){
                        Log.d("errorSaveOrder", "Error: " + firebaseError.getMessage());
                        Toast.makeText(activity,"No se pudo guardar la reserva, intente mas tarde", Toast.LENGTH_SHORT).show();
                    }else{
                        Log.d("Orders status:","Save Successful Firebase");
                        DownloadOrdersAsyncTask downloadOrdersAsyncTask = new DownloadOrdersAsyncTask(activity, progressHUD, dialogReservation);
                        downloadOrdersAsyncTask.execute("Descargar Ordenes");
                    }
                    if(progressHUD != null) {
                        progressHUD.setVisibility(View.INVISIBLE);

                    }
                }
            });
        }catch (Exception ex){
            Log.e("RegisterUser",ex.getMessage());
        }
        return true;
    }
}
