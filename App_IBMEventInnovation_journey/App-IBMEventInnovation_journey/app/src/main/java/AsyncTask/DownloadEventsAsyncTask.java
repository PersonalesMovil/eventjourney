package AsyncTask;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.selectacg.daniel.ibmeventinnovation.Controllers.PedidosController;
import com.selectacg.daniel.ibmeventinnovation.R;

import java.util.ArrayList;

import Model.Events;
import Model.FireBase;
import Model.Users;
import Resource.Connectivity;
import Resource.DataUser;
import Resource.MetodosUtiles;

/**
 * Created by Daniel on 20/06/16.
 */
public class DownloadEventsAsyncTask extends AsyncTask<String, String, String> {

    //Se solicita la actividad actual de la aplicacion para acceder el main activity
    Activity activity;
    RelativeLayout progressHUD;
    TextView mensajeTextView;
    Users lect;

    @Override
    protected String doInBackground(String... params) {
        try {
            publishProgress(activity.getString(R.string.msj_verified_network));
            if (Connectivity.isConnectedNetWork(activity)) {
                getAllEvents();
            } else {
                publishProgress(activity.getString(R.string.msj_is_it_not_connected_network));
            }
        } catch (Exception ex) {
            publishProgress("Error:" + ex.getMessage());
            MetodosUtiles.mostrarPausaMensaje();
        }
        return "Validacion exitosa";
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressHUD.setVisibility(View.VISIBLE);
        //contentPage.setVisibility(View.INVISIBLE);
        mensajeTextView = (TextView) progressHUD.findViewById(R.id.mensajeTextView);
        mensajeTextView.setText(activity.getString(R.string.msj_wait));
    }

    @Override
    protected void onPostExecute(String s) {
        try {
            if (lect != null) {
                super.onPostExecute(s);
            } else {
                progressHUD.setVisibility(View.INVISIBLE);
            }
        } catch (Exception ex) {
            Toast.makeText(activity, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        mensajeTextView.setText(values[0]);
    }

    public DownloadEventsAsyncTask(Activity activity, RelativeLayout progressHUD) {
        super();
        this.activity = activity;
        this.progressHUD = progressHUD;
    }


    public void getAllEvents(){
        FireBase myFireBase = new FireBase();
        myFireBase.getUrlEvents().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                try {
                    ArrayList<Events> listEvents = new ArrayList<>();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        Events event = postSnapshot.getValue(Events.class);
                        listEvents.add(event);
                    }
                    if (listEvents.size() > 0) {
                        Log.d("Events Download:"," "+ listEvents.size());
                        DataUser dataUser = DataUser.newInstance();
                        //Se debe llamar la actividad que mostrara la lista de dias programados
                        Intent MandantSelection = new Intent(activity, PedidosController.class);
                        activity.startActivity(MandantSelection);
                        activity.finish();
                    }
                }catch (Exception ex) {
                    Log.e("LoadEvents", ex.getMessage());
                }
            }
            @Override public void onCancelled(FirebaseError error) {
                System.out.println("Details: " + error.getDetails());
            }
        });
    }

}