package AsyncTask;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.selectacg.daniel.ibmeventinnovation.R;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import Model.FireBase;
import Model.Users;
import Resource.Connectivity;
import Resource.MetodosUtiles;

/**
 * Created by Daniel on 20/06/16.
 */
public class SaveUserAsyncTask extends AsyncTask<Users, String, String> {

    //Se solicita la actividad actual de la aplicacion para acceder el main activity
    Activity activity;
    RelativeLayout progressHUD;
    TextView mensajeTextView;
    Users lect;

    @Override
    protected String doInBackground(Users... params) {
        lect = params[0];
        try {
            publishProgress(activity.getString(R.string.msj_verified_network));
            if (Connectivity.isConnectedNetWork(activity)) {
                saveUser(lect);
            } else {
                publishProgress(activity.getString(R.string.msj_is_it_not_connected_network));
            }
        } catch (Exception ex) {
            publishProgress("Error:" + ex.getMessage());
            MetodosUtiles.mostrarPausaMensaje();
        }
        return "Validacion exitosa";
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressHUD.setVisibility(View.VISIBLE);
        //contentPage.setVisibility(View.INVISIBLE);
        mensajeTextView = (TextView) progressHUD.findViewById(R.id.mensajeTextView);
        mensajeTextView.setText(activity.getString(R.string.msj_wait));
    }

    @Override
    protected void onPostExecute(String s) {
        try {
            if (lect != null) {
                super.onPostExecute(s);
            } else {
                progressHUD.setVisibility(View.INVISIBLE);
            }
        } catch (Exception ex) {
            Toast.makeText(activity, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        mensajeTextView.setText(values[0]);
    }

    public SaveUserAsyncTask(Activity activity,RelativeLayout progressHUD) {
        super();
        this.activity = activity;
        this.progressHUD = progressHUD;
    }

    public Boolean saveUser(Users user){
        try {
            user.Hour_Connection = MetodosUtiles.date_format(activity.getString(R.string.format_date));
            FireBase myFireBase = new FireBase();
            myFireBase.getUrlUsers().push().setValue(user, new Firebase.CompletionListener() {
                @Override
                public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                    if(firebaseError != null){
                        Log.e("errorSaveuser", firebaseError.getMessage());
                    }else{
                        Log.d("User save sucessful:","Firebase");


                    }
                    progressHUD.setVisibility(View.INVISIBLE);
                }
            });
        }catch (Exception ex){
            Log.e("RegisterUser",ex.getMessage());
        }
        return true;
    }

    public Boolean updateUser(Users user){
        try {
            user.Hour_Connection = MetodosUtiles.date_format(activity.getString(R.string.format_date));
            FireBase myFireBase = new FireBase();
            Map<String, Object> Users = new HashMap<>();
            Users.put("Email", user.Email);
            Users.put("Id", user.Id);
            Users.put("Company", user.Company);
            Users.put("Gender", user.Gender);
            Users.put("Hour_Connection", user.Hour_Connection);
            Users.put("Name", user.Name);
            Users.put("Last_Name", user.Last_Name);
            myFireBase.getUrlUsers().updateChildren(Users, new Firebase.CompletionListener() {
                @Override
                public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                    if(firebaseError != null){
                        Log.e("Users: ","Error save User " + firebaseError.getMessage());
                    }else{
                        Log.d("Users: ","save sucessful Firebase");
                    }
                    progressHUD.setVisibility(View.INVISIBLE);
                }
            });
        }catch (Exception ex){
            Log.e("RegisterUser",ex.getMessage());
        }
        return true;
    }
}