package AsyncTask;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.selectacg.daniel.ibmeventinnovation.Controllers.HistorialesController;
import com.selectacg.daniel.ibmeventinnovation.Controllers.PedidosController;
import com.selectacg.daniel.ibmeventinnovation.R;

import java.util.ArrayList;

import Model.FireBase;
import Model.Orders;
import Model.Records;
import Model.Users;
import Resource.Connectivity;
import Resource.DataUser;
import Resource.MetodosUtiles;
import Resource.PrefUtils;

/**
 * Created by Daniel on 23/07/16.
 */
public class DownloadRecordsAsyncTask extends AsyncTask<String, String, String> {

    //Se solicita la actividad actual de la aplicacion para acceder el main activity
    Activity activity;
    RelativeLayout progressHUD;
    TextView mensajeTextView;

    @Override
    protected String doInBackground(String... params) {
        try {
            publishProgress(activity.getString(R.string.msj_verified_network));
            if (Connectivity.isConnectedNetWork(activity)) {
                publishProgress(activity.getString(R.string.msj_download_records));
                getAllRecords();
            } else {
                publishProgress(activity.getString(R.string.msj_is_it_not_connected_network));
            }
        } catch (Exception ex) {
            publishProgress("Error:" + ex.getMessage());
            MetodosUtiles.mostrarPausaMensaje();
        }
        return "Validacion exitosa";
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressHUD.setVisibility(View.VISIBLE);
        mensajeTextView = (TextView) progressHUD.findViewById(R.id.mensajeTextView);
        mensajeTextView.setText(activity.getString(R.string.msj_wait));
    }

    @Override
    protected void onPostExecute(String s) {
        try {
            super.onPostExecute(s);
        } catch (Exception ex) {
           Log.d("DownloadRecords","Error: " + ex.getMessage());
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        mensajeTextView.setText(values[0]);
    }

    public DownloadRecordsAsyncTask(Activity activity, RelativeLayout progressHUD) {
        super();
        this.activity = activity;
        this.progressHUD = progressHUD;
    }

    public void getAllRecords(){
        FireBase myFireBase = new FireBase();
        myFireBase.getUrlRecords().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                try {
                    ArrayList<Records> listRecord = new ArrayList<>();
                    Users currenUser = PrefUtils.getCurrentUser(activity);;

                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        Records records = postSnapshot.getValue(Records.class);
                        if(records.email.trim().equalsIgnoreCase(currenUser.Email.trim())) {
                            listRecord.add(records);
                        }
                    }
                    Log.d("Record Download:"," "+ listRecord.size());
                    if (listRecord.size() > 0) {
                        DataUser dataUser = DataUser.newInstance();
                        dataUser.recordsList = listRecord;
                        //Se debe llamar la actividad que mostrara la lista de dias programados
                        progressHUD.setVisibility(View.INVISIBLE);
                        Intent MandantSelection = new Intent(activity, HistorialesController.class);
                        activity.startActivity(MandantSelection);
                    }else{
                        progressHUD.setVisibility(View.INVISIBLE);
                        Toast.makeText(activity,"No se encontro datos de historial",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception ex) {
                    Log.d("LoadEvents","Error: " + ex.getMessage());
                    progressHUD.setVisibility(View.INVISIBLE);
                }
            }
            @Override public void onCancelled(FirebaseError error) {
                System.out.println("Details: " + error.getDetails());
            }
        });
    }
}