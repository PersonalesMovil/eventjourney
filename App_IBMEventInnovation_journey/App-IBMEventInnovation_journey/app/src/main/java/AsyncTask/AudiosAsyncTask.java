package AsyncTask;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.util.Log;

import com.selectacg.daniel.ibmeventinnovation.R;

/**
 * Created by Daniel on 3/08/16.
 */
public class AudiosAsyncTask   extends AsyncTask<Boolean, Void, String> {

    Context activity;

    public AudiosAsyncTask(Context activity) {
       this.activity = activity;
    }

    // Decode image in background.
    @Override
    protected String doInBackground(Boolean... params) {
        if(params[0]){
            reproducirSonidoBienvenida();
        }else{
            reproducirSonidoDespedida();
        }
        return "";
    }

    // Once complete, see if ImageView is still around and set bitmap.
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
    }

    private void reproducirSonidoDespedida() {
        try{
            final MediaPlayer mp = MediaPlayer.create(activity, R.raw.a_006);
            mp.start();
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.stop();
                    mp.release();

                };
            });

        }catch (Exception ex){
            Log.d("Error","MediaPlayer: " +ex.getMessage());
        }
    }

    private void reproducirSonidoBienvenida(){
        try{
            final MediaPlayer mp = MediaPlayer.create(activity, R.raw.ste_20);
            mp.start();
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.stop();
                    mp.release();

                };
            });
        }catch (Exception ex) {
            Log.d("Error", "MediaPlayer: " + ex.getMessage());
        }
    }

}
