package AsyncTask;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.selectacg.daniel.ibmeventinnovation.R;

import java.util.ArrayList;

import Model.FireBase;
import Model.Routers;
import Model.Users;
import Resource.Connectivity;
import Resource.DataUser;
import Resource.MetodosUtiles;

/**
 * Created by Daniel on 20/06/16.
 */
public class DownloadRoutersAsyncTask  extends AsyncTask<String, String, String> {

    //Se solicita la actividad actual de la aplicacion para acceder el main activity
    Activity activity;
    RelativeLayout progressHUD;
    TextView mensajeTextView;
    Users lect;

    @Override
    protected String doInBackground(String... params) {
        try {
            publishProgress(activity.getString(R.string.msj_verified_network));
            if (Connectivity.isConnectedNetWork(activity)) {
                getAllRouters();
            } else {
                publishProgress(activity.getString(R.string.msj_is_it_not_connected_network));
            }
        } catch (Exception ex) {
            publishProgress("Error:" + ex.getMessage());
            MetodosUtiles.mostrarPausaMensaje();
        }
        return "Validacion exitosa";
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressHUD.setVisibility(View.VISIBLE);
        //contentPage.setVisibility(View.INVISIBLE);
        mensajeTextView = (TextView) progressHUD.findViewById(R.id.mensajeTextView);
        mensajeTextView.setText(activity.getString(R.string.msj_wait));
    }

    @Override
    protected void onPostExecute(String s) {
        try {
            if (lect != null) {
                super.onPostExecute(s);
            } else {
                progressHUD.setVisibility(View.INVISIBLE);
            }
        } catch (Exception ex) {
            Toast.makeText(activity, "Error: " + ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        mensajeTextView.setText(values[0]);
    }

    public DownloadRoutersAsyncTask(Activity activity, RelativeLayout progressHUD) {
        super();
        this.activity = activity;
        this.progressHUD = progressHUD;
    }

    public void getAllRouters(){
        FireBase myFireBase = new FireBase();
        myFireBase.getUrlRouters().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                try {
                    ArrayList<Routers> listRouters = new ArrayList<>();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        Routers routers = postSnapshot.getValue(Routers.class);
                        listRouters.add(routers);
                    }
                    if (listRouters.size() > 0) {
                        Log.d("Routers Download:"," "+ listRouters.size());
                        DataUser dataUser = DataUser.newInstance();
                        dataUser.routersList = listRouters;
                    }
                }catch (Exception ex) {
                    Log.e("LoadEvents", ex.getMessage());
                }
            }
            @Override public void onCancelled(FirebaseError error) {
                System.out.println("Details: " + error.getDetails());
            }
        });
    }
}
